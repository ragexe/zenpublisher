-- MySQL dump 10.13  Distrib 5.6.25, for Win64 (x86_64)
--
-- Host: localhost    Database: bd_dzen
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `answr_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Answer ID',
  `answr_usr_nickname` varchar(26) NOT NULL COMMENT 'Nickname of author (User). many-to-one',
  `answr_answr_id` int(11) DEFAULT NULL COMMENT 'ID of parent Answer. many-to-one',
  `answr_quest_id` int(11) NOT NULL COMMENT 'ID of Question whict to Answer belongs. many-to-one',
  `answr_datetime_crtn` datetime DEFAULT NULL COMMENT 'Date and Time of creation',
  `answr_body` mediumtext NOT NULL COMMENT 'Text of Answer',
  `answr_datetime_upd` datetime DEFAULT NULL COMMENT 'Date and Time of update',
  `answr_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  PRIMARY KEY (`answr_id`),
  UNIQUE KEY `f_answr_id_UNIQUE` (`answr_id`),
  KEY `f_answr_answr_id_idx` (`answr_answr_id`),
  KEY `f_answr_usr_id_idx` (`answr_usr_nickname`),
  KEY `f_answr_quest_id_idx` (`answr_quest_id`),
  CONSTRAINT `answr_usr_nickname` FOREIGN KEY (`answr_usr_nickname`) REFERENCES `user` (`usr_nickname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='Table of Answers';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (0,'mrs',1,1,'2016-07-26 23:42:04','Я не могу сказать, т.к. не знаю сам.2',NULL,'\0'),(1,'gentleman',-1,1,'2016-07-18 20:57:20','Я не могу сказать, т.к. не знаю сам.',NULL,'\0'),(2,'gentleman',-1,2,'2016-07-26 23:36:52','Чупакабра',NULL,'\0'),(3,'ragexe',-1,2,'2016-07-26 23:36:52','Нормальный комментарий','2016-09-25 22:14:26','\0'),(4,'ragexe',-1,0,'2016-09-13 15:11:27','Оставить комментарий',NULL,'\0'),(5,'ragexe',-1,1,'2016-09-13 15:28:55','213',NULL,'\0'),(6,'ragexe',-1,1,'2016-09-13 15:29:41','Да не бред',NULL,'\0'),(7,'ragexe',-1,0,'2016-09-13 17:52:41','dasda',NULL,'\0'),(8,'ragexe',-1,1,'2016-09-13 17:57:03','dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',NULL,'\0'),(9,'ragexe',-1,1,'2016-09-13 17:57:21','dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',NULL,'\0'),(10,'ragexe',4,0,'2016-09-14 14:17:38','Комментарий к комментарию',NULL,'\0'),(11,'ragexe',10,0,'2016-09-14 14:18:11','Комментарий к комментарию к комментарию',NULL,'\0'),(12,'ragexe',11,0,'2016-09-14 14:19:22','Комментарий к комментарию к комментарию к комментарию\r\n',NULL,'\0'),(13,'ragexe',-1,0,'2016-09-14 14:19:47','Нет',NULL,'\0'),(14,'ragexe',-1,0,'2016-09-14 14:20:09','фывф',NULL,'\0'),(15,'mrs',11,0,'2016-09-14 14:20:54','Слишком сложно',NULL,'\0'),(16,'mrs',-1,0,'2016-09-14 14:26:00','df',NULL,'\0'),(17,'ragexe',-1,20,'2016-09-16 21:32:04','Я написал',NULL,'\0'),(18,'ragexe',-1,2,'2016-09-21 18:25:01','Еще один нормальный комментарий','2016-09-25 22:19:41','\0'),(19,'ragexe',-1,2,'2016-09-21 18:25:36','dasda',NULL,'\0'),(20,'mrs',-1,2,'2016-09-23 21:06:25','ваЛжАОДэЛДжП',NULL,'\0'),(21,'mrs',-1,2,'2016-09-23 21:18:00','Брантозаврик',NULL,'\0'),(22,'mrs',-1,2,'2016-09-23 21:22:12','Привет',NULL,'\0'),(23,'mrs',2,2,'2016-09-23 21:22:18','Сияние',NULL,'\0'),(24,'ragexe',-1,3,'2016-09-25 14:54:39','по жизни прям',NULL,'\0'),(29,'ragexe',2,2,'2016-09-25 16:22:54','1',NULL,'\0'),(30,'ragexe',29,2,'2016-09-25 16:22:58','2',NULL,'\0'),(31,'ragexe',30,2,'2016-09-25 16:23:03','3',NULL,'\0'),(32,'ragexe',23,2,'2016-09-25 22:45:19','Проверка',NULL,'\0'),(33,'mrs',-1,2,'2016-09-28 13:58:29','Так',NULL,'\0'),(34,'ragexe',-1,3,'2016-09-28 20:33:23','2',NULL,'\0'),(35,'ragexe',-1,3,'2016-09-28 20:33:32','3',NULL,'\0'),(36,'ragexe',-1,3,'2016-09-28 20:33:43','4',NULL,'\0'),(37,'ragexe',-1,3,'2016-09-28 20:34:25','5',NULL,'\0'),(38,'ragexe',-1,3,'2016-09-28 20:34:40','6',NULL,'\0'),(39,'ragexe',-1,3,'2016-09-28 20:39:06','7',NULL,'\0'),(40,'ragexe',39,3,'2016-09-29 00:59:44','8',NULL,'\0'),(41,'ragexe',39,3,'2016-09-29 00:59:59','9',NULL,'\0'),(42,'ragexe',39,3,'2016-09-29 01:00:30','10',NULL,'\0'),(43,'ragexe',34,3,'2016-09-29 01:02:48','11',NULL,'\0'),(44,'ragexe',40,3,'2016-09-29 13:34:32','test',NULL,'\0'),(45,'ragexe',34,3,'2016-09-29 13:35:47','test',NULL,'\0'),(46,'ragexe',43,3,'2016-09-29 13:35:57','test',NULL,'\0'),(47,'ragexe',17,20,'2016-09-29 14:14:43','test','2016-09-30 23:39:25','\0'),(48,'ragexe',-1,21,'2016-09-29 16:25:02','Штангенциркуль','2016-09-30 22:47:26','\0'),(49,'ragexe',-1,21,'2016-09-29 21:46:24','фаыв',NULL,''),(51,'ragexe',-1,20,'2016-10-01 00:28:50','test',NULL,''),(52,'ragexe',-1,21,'2016-10-01 02:46:31','testing answers',NULL,''),(53,'ragexe',52,21,'2016-10-01 02:46:43','test',NULL,'\0'),(54,'mrs',-1,22,'2016-10-01 22:20:34','test',NULL,'\0'),(55,'rocknroll',54,22,'2016-10-01 22:21:59','test test','2016-10-01 22:22:26','\0'),(56,'ragexe',-1,21,'2016-10-02 14:54:56','4gb оперативной памяти)',NULL,'\0');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `categr_id` int(11) NOT NULL COMMENT 'Category ID',
  `categr_name` varchar(45) NOT NULL COMMENT 'Category Name',
  `categr_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  PRIMARY KEY (`categr_id`),
  UNIQUE KEY `f_categr_id_UNIQUE` (`categr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table of all questions categories. one(category)-to-many(question)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Авто, Мото','\0'),(2,'Бизнес, Финансы','\0'),(3,'Города и Страны','\0'),(4,'Досуг, Развлечения','\0'),(5,'Еда, Кулинария','\0'),(6,'Животные, Растения','\0'),(7,'Искусство и Культура','\0'),(8,'Видеоигры','\0'),(9,'Компьютеры, Связь','\0'),(10,'Наука, Техника , Языки','\0'),(11,'Образование','\0'),(12,'Программирование','\0'),(13,'Путешествия, Туризм','\0'),(14,'Работа, Карьера','\0'),(15,'Семья, Дом. Дети','\0'),(16,'Спорт','\0'),(17,'Стиль, Мода, Звезды','\0'),(18,'Юридическая консультация','\0'),(19,'Юмор','\0'),(20,'Прочее','\0');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fame_answer`
--

DROP TABLE IF EXISTS `fame_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fame_answer` (
  `fame_answr_answr_id` int(11) NOT NULL COMMENT 'Answer ID. many-to-many\n',
  `fame_answr_usr_nickname` varchar(26) NOT NULL COMMENT 'User ID. many-to-many',
  `fame_answr_mark` tinyint(10) DEFAULT NULL COMMENT 'If User estimate Answer with mark from 0 to 10.',
  `fame_answr_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  `fame_answr_datetime` datetime NOT NULL COMMENT 'Date of User''s mark',
  PRIMARY KEY (`fame_answr_answr_id`,`fame_answr_usr_nickname`),
  KEY `f_fame_answr_answr_id_idx` (`fame_answr_answr_id`),
  KEY `fame_answr_usr_nickname_idx` (`fame_answr_usr_nickname`),
  CONSTRAINT `fame_answr_answr_id` FOREIGN KEY (`fame_answr_answr_id`) REFERENCES `answer` (`answr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fame_answr_usr_nickname` FOREIGN KEY (`fame_answr_usr_nickname`) REFERENCES `user` (`usr_nickname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table with "likes" and "dislikes" of Users about the Answers\n';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fame_answer`
--

LOCK TABLES `fame_answer` WRITE;
/*!40000 ALTER TABLE `fame_answer` DISABLE KEYS */;
INSERT INTO `fame_answer` VALUES (0,'mrKrabby',10,'\0','2016-07-15 00:00:00'),(0,'ragexe',10,'\0','2016-09-16 13:47:42'),(1,'ragexe',1,'\0','2016-07-26 23:00:00'),(2,'ragexe',10,'\0','2016-07-26 14:00:00'),(4,'ragexe',10,'\0','2016-09-19 23:02:28'),(5,'mrs',1,'\0','2016-09-16 14:05:18'),(5,'ragexe',10,'\0','2016-09-16 14:05:00'),(10,'ragexe',3,'\0','2016-09-16 13:50:53'),(11,'ragexe',10,'\0','2016-09-16 13:56:57'),(12,'ragexe',3,'\0','2016-09-19 23:02:32'),(17,'ragexe',9,'\0','2016-09-16 21:40:44'),(47,'ragexe',3,'\0','2016-10-01 00:00:11'),(54,'ragexe',2,'\0','2016-10-01 22:20:51');
/*!40000 ALTER TABLE `fame_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fame_quest`
--

DROP TABLE IF EXISTS `fame_quest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fame_quest` (
  `fame_quest_quest_id` int(11) NOT NULL COMMENT 'Question ID. many-to-many',
  `fame_quest_usr_nickname` varchar(26) NOT NULL COMMENT 'User ID. many-to-many',
  `fame_quest_mark` tinyint(10) DEFAULT NULL COMMENT 'If User likes Question - 1, else - 0;',
  `fame_quest_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  `fame_quest_datetime` datetime NOT NULL COMMENT 'Date of User''s mark',
  PRIMARY KEY (`fame_quest_quest_id`,`fame_quest_usr_nickname`),
  KEY `fame_quest_id_idx` (`fame_quest_quest_id`),
  KEY `fame_quest_usr_nickname_idx` (`fame_quest_usr_nickname`),
  CONSTRAINT `fame_quest_quest_id` FOREIGN KEY (`fame_quest_quest_id`) REFERENCES `question` (`quest_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fame_quest_usr_nickname` FOREIGN KEY (`fame_quest_usr_nickname`) REFERENCES `user` (`usr_nickname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table with "likes" and "dislikes" of Users about the Questions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fame_quest`
--

LOCK TABLES `fame_quest` WRITE;
/*!40000 ALTER TABLE `fame_quest` DISABLE KEYS */;
INSERT INTO `fame_quest` VALUES (1,'mrKrabby',3,'\0','2016-01-12 00:00:00'),(1,'ragexe',1,'\0','2015-10-04 00:00:00'),(2,'ragexe',10,'\0','2016-07-26 00:00:00'),(3,'mrKrabby',5,'\0','2016-06-15 00:00:00'),(3,'ragexe',1,'\0','2016-09-29 21:47:47'),(3,'rocknroll',10,'\0','2016-09-20 14:49:56'),(11,'ragexe',6,'\0','2016-09-16 13:15:15'),(13,'ragexe',1,'\0','2016-09-16 20:45:38'),(14,'ragexe',3,'\0','2016-09-16 20:45:23'),(20,'ragexe',10,'\0','2016-10-01 00:00:33'),(21,'ragexe',5,'\0','2016-10-01 01:11:22'),(22,'mrs',1,'\0','2016-10-01 22:20:22');
/*!40000 ALTER TABLE `fame_quest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `quest_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Question ID',
  `quest_categr_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID of Question''s Category. Default - "Прочее" with ID 0. many-to-one\n',
  `quest_title` varchar(45) NOT NULL DEFAULT 'Title' COMMENT 'Title of the Question',
  `quest_body` mediumtext NOT NULL COMMENT 'Text of the actual Question',
  `quest_usr_nickname` varchar(26) NOT NULL COMMENT 'Nickname of Question''s author (User). many-to-one',
  `quest_datetime_crtn` datetime NOT NULL COMMENT 'Date and Time of creation',
  `quest_date_upd` datetime DEFAULT NULL,
  `quest_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  PRIMARY KEY (`quest_id`),
  UNIQUE KEY `f_quest_id_UNIQUE` (`quest_id`),
  KEY `f_quest_categr_id_idx` (`quest_categr_id`),
  KEY `quest_usr_nickname_idx` (`quest_usr_nickname`),
  CONSTRAINT `quest_categr_id` FOREIGN KEY (`quest_categr_id`) REFERENCES `category` (`categr_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `quest_usr_nickname` FOREIGN KEY (`quest_usr_nickname`) REFERENCES `user` (`usr_nickname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Table of question';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,1,'Права','С какого возраста можно пойти учиться на права на скутер?','ragexe','2016-07-24 19:29:37',NULL,''),(2,1,'Преды','Подскажите плз, где находится коробка с предохранителями в запорожце?','mrKrabby','2016-07-26 20:33:37',NULL,'\0'),(3,2,'Победитель','Вложил все деньги в Forex. Кто я?','mrs','0000-00-00 00:00:01','2016-09-25 13:43:29','\0'),(6,2,'Тайтл','Текст вопроса?','ragexe','2016-08-15 00:00:00',NULL,'\0'),(7,4,'Заголовок','Текст вопроса?','ragexe','2016-09-12 21:57:32',NULL,'\0'),(11,8,'Овервоч','Баката','ragexe','2016-09-13 08:58:33',NULL,'\0'),(12,19,'Заголовок типа','Текст вопроса типа','ragexe','2016-09-13 09:21:29',NULL,'\0'),(13,17,'11111111','22222222','ragexe','2016-09-13 09:33:43',NULL,'\0'),(14,1,'333333333333','222222222222','ragexe','2016-09-13 09:46:21',NULL,'\0'),(17,6,'Капибара','Почему капибара?','ragexe','2016-09-13 10:16:03',NULL,'\0'),(18,4,'Василий','Привет!','ragexe','2016-09-13 10:23:41',NULL,'\0'),(19,11,'Образование налета','Что?','ragexe','2016-09-13 11:40:50',NULL,'\0'),(20,1,'БАката','Баката','ragexe','2016-09-16 21:31:44',NULL,'\0'),(21,17,'Подарки','Что подарить мужу на день рождения, если он попросил  штангенциркуль?\r\n','karin','2016-09-20 22:17:33',NULL,'\0'),(22,1,'test','test','ragexe','2016-10-01 21:08:14',NULL,'');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `usr_nickname` varchar(26) NOT NULL COMMENT 'Nickname of User',
  `usr_role` enum('COMMON','ADMIN') NOT NULL DEFAULT 'COMMON' COMMENT 'Role of User: COMMON - common user, ADMIN - user with root-power',
  `usr_date_crtn` date NOT NULL COMMENT 'Date of creation of user''s account',
  `usr_del` bit(1) NOT NULL DEFAULT b'0' COMMENT 'is Deleted? 1 - true, 0 - false',
  `usr_date_lactivity` datetime DEFAULT NULL COMMENT 'DateTime of last User activity',
  `usr_password` varchar(45) NOT NULL,
  PRIMARY KEY (`usr_nickname`),
  UNIQUE KEY `usr_nickname_UNIQUE` (`usr_nickname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table of Users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('gentleman','COMMON','2016-07-18','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('karin','COMMON','2016-09-30','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('mrKrabby','COMMON','2016-07-18','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('mrs','COMMON','2016-07-18','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('Nick2name','COMMON','2016-07-26','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('ragexe','ADMIN','2016-07-16','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('ragexe1','COMMON','2016-09-17','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('rocknroll','COMMON','2016-09-20','\0',NULL,'81dc9bdb52d04dc20036dbd8313ed055'),('test','COMMON','2016-09-30','\0',NULL,'098f6bcd4621d373cade4e832627b4f6');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-02 15:56:33
