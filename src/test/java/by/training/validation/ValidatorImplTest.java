package by.training.validation;


import by.training.entity.Answer;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class ValidatorImplTest extends TestCase {


    public void testValidateListOfAnswers() throws Exception {
        List<Answer> answers = new ArrayList<>();

        Answer answer1 = new Answer(1, 2);
        Answer answer2 = new Answer(2, 3);
        Answer answer3 = new Answer(3, 4);
        Answer answer4 = new Answer(4, 5);
        Answer answer5 = new Answer(5, 6);
        Answer answer6 = new Answer(6, -1);
        Answer answer7 = new Answer(7, 9);//invalid
        Answer answer8 = new Answer(8, 9);//invalid

        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);
        answers.add(answer4);
        answers.add(answer5);
        answers.add(answer6);
        answers.add(answer7);
        answers.add(answer8);

        IValidator validator = new ValidatorImpl();

        List<Answer> resultAnswers = validator.validateListOfAnswers(answers);
        answers.remove(answer7);
        answers.remove(answer8);

        assertEquals(true, answers.equals(resultAnswers));
    }

    public void testValidateLanguage() throws Exception {
        IValidator validator = new ValidatorImpl();

        String language = "1ру1";
        language = validator.validateLanguage(language);

        assertEquals(true, language.equals("ru"));

    }
}