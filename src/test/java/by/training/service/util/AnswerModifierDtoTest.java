package by.training.service.util;

import by.training.dto.AnswerDTO;
import by.training.entity.Answer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AnswerModifierDtoTest {

    @Test
    public void modify() throws Exception {
        AnswerModifierDto answerModifierDto = new AnswerModifierDto();
//        answerModifierDto.modify()

        List<Answer> answers = new ArrayList<>();


        Answer answer3 = new Answer(3, 7);
        Answer answer4 = new Answer(4, 5);
        Answer answer5 = new Answer(5, 7);
        Answer answer6 = new Answer(6, 7);
        Answer answer7 = new Answer(7, -1);
        /*
        7
        6   5   3
            4

         */

        answers.add(answer3);
        answers.add(answer4);
        answers.add(answer5);
        answers.add(answer6);
        answers.add(answer7);

        List<AnswerDTO> answerDTOs = answerModifierDto.modify(answers);

        List<AnswerDTO> answerDTOtest = new ArrayList<>();
        answerDTOtest.add(new AnswerDTO(answer7));
        answerDTOtest.get(0).addChild(new AnswerDTO(answer6));
        answerDTOtest.get(0).addChild(new AnswerDTO(answer5));
        answerDTOtest.get(0).addChild(new AnswerDTO(answer3));
        answerDTOtest.get(0).getAnswerChildList().get(1).addChild(answer4);

        assertEquals(true, answerDTOs.equals(answerDTOtest));


    }
}