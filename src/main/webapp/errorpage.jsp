<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<core:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />
<jsp:useBean id="timeValues" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title><fmt:message key="error.page.title"/></title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="/WEB-INF/jsp/common/style.jsp"/>
    </head>

    <body>
        <%-- Nav Bar --%>
        <jsp:include page="WEB-INF/jsp/common/navbar.jsp"/>

        <%-- Content --%>
        <div class="container blog-content">
            <div class="row">
                <div class="col-md-12 blog-main">
                    <section class="blog-post">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="blog-post-meta" style="background: url(//www.google.com/images/errors/robot.png) 100% 10% no-repeat;">
                                    <div>
                                        <h3 style="color: red"><fmt:message key="error.page.header"/></h3>
                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <p>${pageContext.errorData.requestURI} <fmt:message key="error.page.msg"/></p>
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td><fmt:message key="error.page.path"/></td>
                                                <td>${pageContext.request.contextPath}</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">2</th>
                                                <td><fmt:message key="error.page.servlet"/></td>
                                                <td>${pageContext.errorData.servletName}</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">3</th>
                                                <td><fmt:message key="error.page.content_type"/></td>
                                                <td>${pageContext.response.contentType}</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">4</th>
                                                <td><fmt:message key="error.page.encoding"/></td>
                                                <td>${pageContext.response.characterEncoding}</td>
                                            </tr>

                                            <tr>
                                                <core:set target="${timeValues}" value="${pageContext.request.session.creationTime}"
                                                          property="time"/>
                                                <th scope="row">5</th>
                                                <td><fmt:message key="error.page.session_id"/></td>
                                                <td>${pageContext.request.session.id}</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">6</th>
                                                <td><fmt:message key="error.page.session_crtn"/></td>
                                                <td><fmt:formatDate type="both" value="${timeValues}"/></td>
                                            </tr>

                                            <tr>
                                                <core:set target="${timeValues}" value="${pageContext.request.session.lastAccessedTime}"
                                                          property="time"/>
                                                <th scope="row">7</th>
                                                <td><fmt:message key="error.page.session_inv"/></td>
                                                <td><fmt:formatDate type="both" value="${timeValues}"/></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">8</th>
                                                <td><fmt:message key="error.page.status"/></td>
                                                <td><%--${pageContext.errorData.statusCode}--%> <strong>${pageContext.response.status}</strong></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">9</th>
                                                <td><fmt:message key="error.page.exception"/></td>
                                                <td>${exception}</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">10</th>
                                                <td><fmt:message key="error.page.exception.msg"/></td>
                                                <td>${exception.message}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="btn-group">
                                    <a href="${pageContext.request.contextPath}/main" class="btn btn"><fmt:message key="navbar.footer.home"/></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <%-- Footer --%>
        <jsp:include page="WEB-INF/jsp/common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>
    </body>
</html>
