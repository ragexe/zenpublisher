<%@ tag description="display the whole nodeTree" pageEncoding="UTF-8" %>
<%@ attribute name="answer" type="by.training.dto.AnswerDTO" required="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setBundle basename="text" />

<li class="comment" id="comment-${answer.id}">
    <div id="div-comment-${answer.id}" class="comment-body">
        <div class="commenterImage">
            <img alt="user's avatar" src="${pageContext.request.contextPath}/img/image-4.jpg">
        </div>

        <div class="comment-author vcard">
            <a href="#" class="fn">${answer.usr_nickname}</a> <span class="says"><fmt:message key="comment.says"/></span>
        </div>

        <div class="comment-meta comment-metadata">
            <a href="#comment-${answer.id}">
                <time class="timeago comment-meta comment-metadata" datetime="${answer.datetime_crtn}">
                    $${answer.datetime_crtn}
                </time>
            </a>
        </div>

        <p>${answer.body}</p>

        <div class="comment-footer">
            <div class="btn-group">
                <%--<a class="btn btn-xs ${empty pageContext.request.remoteUser ? 'disabled' : ''}" data-toggle="collapse" data-target="#comment-${answer.id}-collapse">...</a>--%>
                <a class="btn btn-xs" data-toggle="collapse" data-target="#comment-${answer.id}-collapse">...</a>

                <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                    <fmt:message key="main.quest.details.button.mark"/>
                    <span class="badge">${answer.averageMark}</span>
                    <span class="caret"></span>
                    <span class="ripple-container"></span>
                </button>

                <core:if test="${pageContext.request.remoteUser == answer.usr_nickname or pageContext.request.isUserInRole('admin')}">
                    <a href="${pageContext.request.contextPath}/auth?command=edita&id=${answer.id}" class="btn btn-xs"><fmt:message key="main.link.edit"/></a>

                    <a href="${pageContext.request.contextPath}/auth?command=dela&id=${answer.id}" class="btn btn-xs"><fmt:message key="main.link.delete"/></a>
                </core:if>

                <!--rate menu-->
                <ul class="dropdown-menu" role="menu">
                    <core:forEach var="i" begin="1" end="10">
                        <li><a class="${empty pageContext.request.remoteUser ? 'a-auth' : ''}" href="${pageContext.request.contextPath}/auth?command=esta&answer=${answer.id}&mrk=${i}">${i}</a></li>
                    </core:forEach>
                </ul>
            </div>

            <!-- Collapse menu with form -->
            <div id="comment-${answer.id}-collapse" class="collapse">
                <form id="postform-${answer.id}" class="form-group form-auth" method="post" action="${pageContext.request.contextPath}/auth">
                    <label>
                        <input class="hidden" name="comment_id" value="comment-${answer.id}"/>
                    </label>

                    <input type="hidden" name="command" value="ans">
                    <input type="hidden" name="questionId" value="${question.id}"/>
                    <input type="hidden" name="parentCommentId" value="${answer.id}">

                    <div class="form-row">
                        <textarea id="${answer.id}" class="form-control limitInput" placeholder="<fmt:message key="comment.form.placeholder"/>" name="body"></textarea>

                        <span class="help-block" id="help-block-${answer.id}"><!-- func validation --></span>
                    </div>

                    <div class="btn-block margin-top">
                        <button type="submit" form="postform-${answer.id}" id="postform-submit-${answer.id}" disabled class="btn btn-info btn-sm ${empty pageContext.request.remoteUser ? 'btn-auth' : ''}"><fmt:message key="quest.details.share_answer"/></button>

                        <button type="reset" class="btn btn-default btn-sm"><fmt:message key="comment.form.button.clear"/></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <core:if test="${not empty answer.answerChildList}">
        <ul class="children">
            <core:forEach var="child" items="${answer.answerChildList}">
                <template:nodeTree answer="${child}"/>
            </core:forEach>
        </ul>
    </core:if>
</li>

