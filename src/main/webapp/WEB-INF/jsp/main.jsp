<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>

<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>zenPublisher</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>
    </head>

    <body>
        <!-- Nav Bar -->
        <jsp:include page="common/navbar.jsp"/>

        <!-- Content -->
        <div class="container blog-content">
            <div class="row">
                <!-- Sidebar -->
                <jsp:include page="../jsp/common/sidebar.jsp"/>

                <div class="col-md-8 col-md-offset-1 blog-main">
                    <div class="row">
                        <div class="col-md-12">
                            <core:choose>
                                <core:when test="${not empty questionList}">
                                    <core:forEach items="${questionList}" var="question">
                                        <section class="blog-post">
                                            <div class="panel panel-default">
                                                <%-- TODO pic --%>
                                                <%--<img src="${pageContext.request.contextPath}/img/tech/auto.jpg" alt="post following picture" class="img-responsive">--%>

                                                <div class="panel-body">
                                                    <div class="blog-post-meta">
                                                        <div>
                                                            <a href="${pageContext.request.contextPath}/main?cat=${question.categr_id}">
                                                                <span class="label label-light label-cat_id${question.categr_id}">${question.categoryName}</span>
                                                            </a>
                                                        </div>

                                                        <div>
                                                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                                                            
                                                            <a href="${pageContext.request.contextPath}/?command=usr&id=${question.usr_nickname}" class="">${question.usr_nickname}</a>

                                                            <time class="timeago blog-post-date pull-right" datetime="${question.datetime_crtn}">${question.datetime_crtn}</time>
                                                        </div>
                                                    </div>

                                                    <div class="blog-post-content">
                                                        <a href="${pageContext.request.contextPath}/main?command=quest&id=${question.id}">
                                                            <h2 class="blog-post-title">${question.title}</h2>
                                                        </a>
                                                        <p>${question.body}</p>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="btn-group">
                                                        <a class="btn btn-info btn-sm" href="${pageContext.request.contextPath}/main?command=quest&id=${question.id}"><fmt:message key="main.button.detail"/></a>

                                                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle"
                                                                data-toggle="dropdown"
                                                                aria-expanded="false">
                                                            <fmt:message key="main.quest.details.button.mark"/>
                                                            <span class="badge">${question.averageMark}</span>
                                                            <span class="caret"></span>
                                                            <span class="ripple-container"></span>
                                                        </button>

                                                        <ul class="dropdown-menu" role="menu">
                                                            <core:forEach var="i" begin="1" end="10">
                                                                <li><a class="${empty pageContext.request.remoteUser ? 'a-auth' : ''}" href="${pageContext.request.contextPath}/auth?command=estq&quest=${question.id}&mrk=${i}">${i}</a></li>
                                                            </core:forEach>
                                                        </ul>

                                                        <%-- Secured --%>
                                                        <core:if test="${pageContext.request.remoteUser == question.usr_nickname or pageContext.request.isUserInRole('admin')}">
                                                            <a href="${pageContext.request.contextPath}/auth?command=editq&id=${question.id}" class="btn btn-sm"><fmt:message key="main.link.edit"/></a>

                                                            <a href="${pageContext.request.contextPath}/auth?command=delq&id=${question.id}" class="btn btn-sm"><fmt:message key="main.link.delete"/></a>
                                                        </core:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </core:forEach>
                                </core:when>

                                <core:otherwise>
                                    <p><fmt:message key="main.nav.noquestions"/></p>
                                </core:otherwise>
                            </core:choose>

                        </div>
                    </div>

                    <nav>
                        <ul class="pager">
                            <li class="previous ${currentPage != 1 ? '' : 'disabled'}"><a href="?${not empty categoryId ? 'cat='.concat(categoryId).concat('&') : ''}page=${currentPage - 1}">← <fmt:message key="main.nav.button.next"/></a></li>
                            <li class="next ${currentPage != quantityOfPages ? '' : 'disabled'}"><a class="withripple" href="?${not empty categoryId ? 'cat='.concat(categoryId).concat('&') : ''}page=${currentPage + 1}"><fmt:message key="main.nav.button.previous"/> →</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <%-- Snackbar container --%>
        <div id="snackbar-container"></div>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <%-- Snackbars --%>
        <script src="${pageContext.request.contextPath}/libs/js/snackbar.min.js"></script>

        <script type="text/javascript" language="JavaScript"
                src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.${language}.js"></script>

        <%-- TimeAgo --%>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("time.timeago").timeago();
            });
        </script>

        <%-- i18n --%>
        <script>
            var lang = '${language}';
            jQuery.i18n.properties({
                name:'messages',
                path:'libs/bundle/',
                mode:'both',
                language: lang,
                async: true
            });
        </script>

        <%-- Scroll Top Button --%>
        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>

        <%-- snackbar for unauth users --%>
        <script>
            $(document).ready(function () {
                $('.btn-auth').click(function (element) {
                    element.preventDefault();
                    $.snackbar({content: jQuery.i18n.prop('snackbar.comments') + ' <a onclick=\"$(\'#' + this.form.id + '\').submit()\">' + jQuery.i18n.prop('snackbar.continue') + '</a>'});
                });

                $('.a-auth').click(function (element) {
                    element.preventDefault();
                    var href = $(this).attr("href");
                    $.snackbar({content: jQuery.i18n.prop('snackbar.marks') + ' <a href=' + href + '>' + jQuery.i18n.prop('snackbar.continue') + '</a>'});
                });
            });
        </script>

        <%-- Disabled buttons --%>
        <script>
            $('.disabled').click(function(e){
                e.preventDefault();
            })
        </script>
    </body>
</html>
