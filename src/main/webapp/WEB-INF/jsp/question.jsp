<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>zenPublisher</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>
    </head>

    <body>
        <!-- Nav Bar -->
        <jsp:include page="common/navbar.jsp"/>

        <!-- Content -->
        <div class="container blog-content">
            <div class="row">
                <!-- Sidebar -->
                <jsp:include page="../jsp/common/sidebar.jsp"/>

                <div class="col-md-8 col-md-offset-1 blog-main">
                    <%-- Question Section --%>
                    <section class="blog-post">
                        <div class="panel panel-default">
                            <%--<img alt="following post picture" src="${pageContext.request.contextPath}/img/tech/proger.jpg" class="img-responsive">--%>

                            <div class="panel-body">
                                <div class="blog-post-meta">
                                    <div>
                                        <a href="${pageContext.request.contextPath}/main?cat=${question.categr_id}">
                                            <span class="label label-light label-cat_id${question.categr_id}">${question.categoryName}</span>
                                        </a>
                                    </div>

                                    <div>
                                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        <%-- TODO link User's page--%>
                                        <a href="#" class="">${question.usr_nickname}</a>
                                        <time class="timeago blog-post-date pull-right" datetime="${question.datetime_crtn}">${question.datetime_crtn}</time>
                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <h2 class="blog-post-title">${question.title}</h2>

                                    <p>${question.body}</p>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="form-group blog-post-mark">
                                    <%--<form id="postform-${question.id}" action="${pageContext.request.contextPath}/auth?" method="post">--%>
                                    <form id="postform--1" action="${pageContext.request.contextPath}/auth?" method="post">
                                        <input type="hidden" name="command" value="ans"/>

                                        <input type="hidden" name="questionId" value="${question.id}"/>

                                        <input type="hidden" name="parentCommentId" value="-1">

                                        <div class="form-row">
                                            <textarea name="body" id="main" class="form-control limitInput" placeholder="<fmt:message key="quest.details.have_answer"/>"></textarea>

                                            <span class="help-block" id="help-block-main"><!--func--></span>
                                        </div>
                                        <div class="btn-group margin-top">
                                            <button type="submit" form="postform--1" disabled id="postform-submit-main" class="btn btn-info btn-sm postform-submit ${empty pageContext.request.remoteUser ? 'btn-auth' : ''}"><fmt:message key="quest.details.share_answer"/></button>

                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    aria-expanded="false">
                                                <fmt:message key="main.quest.details.button.mark"/>
                                                <span class="badge">${question.averageMark}</span>
                                                <span class="caret"></span>
                                                <span class="ripple-container"></span>
                                            </button>

                                            <ul class="dropdown-menu" role="menu">
                                                <core:forEach var="i" begin="1" end="10">
                                                    <li><a class="${empty pageContext.request.remoteUser ? 'a-auth' : ''}" href="${pageContext.request.contextPath}/auth?command=estq&quest=${question.id}&mrk=${i}">${i}</a></li>
                                                </core:forEach>
                                            </ul>

                                            <%-- Secured --%>
                                            <core:if test="${pageContext.request.remoteUser == question.usr_nickname or pageContext.request.isUserInRole('admin')}">
                                                <a href="${pageContext.request.contextPath}/auth?command=editq&id=${question.id}" class="btn btn-sm"><fmt:message key="main.link.edit"/></a>

                                                <a href="${pageContext.request.contextPath}/auth?command=delq&id=${question.id}" class="btn btn-sm"><fmt:message key="main.link.delete"/></a>
                                            </core:if>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Comments Section -->
                    <section class="blog-comments">
                        <div id="comment_list">
                            <div class="comments-heading"><h2><fmt:message key="quest.details.comments.header"/></h2></div>

                            <ol class="commentlist">
                                <core:choose>
                                    <core:when test="${not empty answers}">
                                        <core:forEach var="child" items="${answers}">
                                            <template:nodeTree answer="${child}"/>
                                        </core:forEach>
                                    </core:when>

                                    <core:otherwise>
                                        <li><p><fmt:message key="quest.details.nocomments"/></p></li>
                                    </core:otherwise>
                                </core:choose>
                            </ol>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <%-- Snackbar container --%>
        <div id="snackbar-container"></div>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <%-- Snackbars --%>
        <script src="${pageContext.request.contextPath}/libs/js/snackbar.min.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.${language}.js"></script>

        <script type="text/javascript" language="JavaScript"
                src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>

        <%-- TimeAgo --%>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("time.timeago").timeago();
            });
        </script>

        <%-- Scroll Top Button --%>
        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>

        <%-- i18n --%>
        <script>
            var lang = '${language}';
            jQuery.i18n.properties({
                name:'messages',
                path:'libs/bundle/',
                mode:'both',
                language: lang,
                async: true
            });
        </script>

        <%-- Snackbar for unauth users --%>
        <script>
            $(document).ready(function () {
                $('.btn-auth').click(function (element) {
                    element.preventDefault();
                    $.snackbar({content: jQuery.i18n.prop('snackbar.comments') + ' <a onclick=\"$(\'#' + this.form.id + '\').submit()\">' + jQuery.i18n.prop('snackbar.continue') + '</a>'});
                });

                $('.a-auth').click(function (element) {
                    element.preventDefault();
                    var href = $(this).attr("href");
                    $.snackbar({content: jQuery.i18n.prop('snackbar.marks') + ' <a href=' + href + '>' + jQuery.i18n.prop('snackbar.continue') + '</a>'});
                });
            });
        </script>

        <%-- Validation Textareas --%>
        <script>
            var max = 255;
            $(document).ready(function () {
                $('.limitInput').keyup(function () {
                    var id = this.id;
                    var count = $(this).val().length;
                    var num = max - count;
                    if ($(this).val().length > 0) {
                        if (num >= 0) {
                            $('#help-block-' + id).text(jQuery.i18n.prop('validation.symbolsLeft')+ ' ' + num).css("color", "");
                            $('#postform-submit-' + id).removeAttr("disabled");
                        } else {
                            $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.achieveLimit') + ' (' + (-num) + ')');
                            $('#postform-submit-' + id).attr("disabled", "true");
                        }
                    } else {
                        $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.noSymbols'));
                        $('#postform-submit-' + id).attr("disabled", "true");
                    }
                });
            });
        </script>
    </body>
</html>
