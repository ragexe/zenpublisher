<%--<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>zenPublisher</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>
    </head>

    <body>
        <!-- Nav Bar -->
        <jsp:include page="common/navbar.jsp"/>

        <!-- Content  -->
        <div class="container blog-content">
            <div class="row">
                <!-- Sidebar -->
                <jsp:include page="../jsp/common/sidebar.jsp"/>

                <div class="col-md-8 col-md-offset-1 blog-main">
                    <div class="row">
                        <div class="col-sm-12">
                            <section class="blog-post">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="blog-post-content">
                                            <h2 class="blog-post-title"><fmt:message key="quest.edit.form.title"/></h2>

                                            <!-- FORM - Edit Question -->
                                            <form id="postform" class="form-horizontal style-form" method="post"
                                                  action="${pageContext.request.contextPath}/auth">
                                                <input type="hidden" name="command" value="UPDATEQ"/>

                                                <input type="hidden" name="id" value="${question.id}"/>

                                                <input type="hidden" name="user" value="${pageContext.request.remoteUser}"/>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"><fmt:message key="quest.form.picture"/></label>

                                                    <div class="col-md-10">
                                                        <div class="input-group image-preview">
                                                            <input type="text" class="form-control image-preview-filename"
                                                                   disabled="disabled"/>
                                                            <!-- don't give a name === doesn't send on POST/GET -->

                                                            <div class="input-group-btn">
                                                                <!-- image-preview-clear button -->
                                                                <button type="button" class="btn btn-default btn-sm image-preview-clear" style="display:none;">
                                                                    <i class="fa fa-times" aria-hidden="true"></i> <fmt:message key="comment.form.button.clear"/>
                                                                </button>

                                                                <!-- image-preview-input -->
                                                                <div class="btn btn-default btn-sm image-preview-input">
                                                                    <i class="fa fa-folder-open" aria-hidden="true"></i>

                                                                    <span class="image-preview-input-title"><fmt:message key="quest.form.button.choose"/></span>

                                                                    <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <%-- Date of publication --%>
                                                <div class="form-group">
                                                    <label for="dateform" class="col-md-2 control-label"><fmt:message key="quest.edit.form.label.date"/></label>

                                                    <div class="col-md-10">
                                                        <input disabled id="dateform" type="datetime" class="form-control" value="${question.datetime_crtn}">
                                                    </div>
                                                </div>

                                                <%-- Category selector --%>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"><fmt:message key="quest.form.label.category"/>*</label>

                                                    <div class="col-md-10">
                                                        <select name="category" autofocus required class="form-control">
                                                            <option value="" disabled><fmt:message key="quest.form.option.category"/></option>

                                                            <core:forEach var="category" items="${categories}">
                                                                <option value="${category.id}" ${category.id == question.categr_id ? 'selected' : ''}>${category.name}</option>
                                                            </core:forEach>
                                                        </select>
                                                    </div>
                                                </div>

                                                <%-- Title --%>
                                                <div class="form-group">
                                                    <label for="titleform" class="col-md-2 control-label"><fmt:message key="quest.form.label.quest_title"/>*</label>

                                                    <div class="col-md-10">
                                                        <input id="titleform" type="text" name="title" class="form-control" value="${question.title}">
                                                    </div>
                                                </div>

                                                <%-- Question body --%>
                                                <div class="form-group">
                                                    <label for="main" class="col-md-2 control-label"><fmt:message key="quest.form.label.quest_body"/>*</label>

                                                    <div class="col-md-10">
                                                        <textarea id="main" name="body" class="form-control limitInput" rows="6"
                                                              placeholder="<fmt:message key="quest.form.textarea.quest_body"/>">${question.body}</textarea>

                                                        <span id="help-block-main"><!-- func validation text --></span>
                                                    </div>
                                                </div>

                                                <%-- Question author --%>
                                                <div class="form-group">
                                                    <label for="dateform" class="col-md-2 control-label"><fmt:message key="quest.edit.form.label.sign"/></label>

                                                    <div class="col-md-10">
                                                        <input disabled id="author" type="text" class="form-control" value="${question.usr_nickname}">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="btn-group">
                                            <input type="submit" disabled id="postform-submit-main" form="postform" value="<fmt:message key="quest.form.button.send"/>"
                                                   class="btn btn-info btn-lg"/>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.${language}.js"></script>

        <script type="text/javascript" language="JavaScript"
                src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>

        <%-- TimeAgo --%>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("time.timeago").timeago();
            });
        </script>

        <%-- Scroll Top Button --%>
        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>

        <%-- i18n --%>
        <script>
            var lang = '${language}';
            jQuery.i18n.properties({
                name:'messages',
                path:'libs/bundle/',
                mode:'both',
                language: lang,
                async: true
            });
        </script>

        <%-- Validation Textareas --%>
        <script>
            var max = 255;
            $(document).ready(function () {
                $('.limitInput').keyup(function () {
                    var id = this.id;
                    var count = $(this).val().length;
                    var num = max - count;
                    if ($(this).val().length > 0) {
                        if (num >= 0) {
                            $('#help-block-' + id).text(jQuery.i18n.prop('validation.symbolsLeft')+ ' ' + num).css("color", "");
                            $('#postform-submit-' + id).removeAttr("disabled");
                        } else {
                            $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.achieveLimit') + ' (' + (-num) + ')');
                            $('#postform-submit-' + id).attr("disabled", "true");
                        }
                    } else {
                        $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.noSymbols'));
                        $('#postform-submit-' + id).attr("disabled", "true");
                    }
                });
            });
        </script>

        <%-- Pic upload --%>
        <script src="${pageContext.request.contextPath}/libs/js/pic_upload.js"></script>
    </body>
</html>
