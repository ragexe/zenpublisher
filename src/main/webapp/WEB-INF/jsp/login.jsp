<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>Login</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>
    </head>

    <body>
        <!-- Nav Bar Custom-->
        <header class="login-navbar-top navbar">
            <div>
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="${pageContext.request.contextPath}/main" title="<fmt:message key="navbar.footer.home"/>">
                            <img class="main-icon" src="${pageContext.request.contextPath}/icons/zenIconInv.png" alt="enso icon">

                            <span>zenPublisher</span>
                        </a>
                    </div>

                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown">
                                <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navbar.categories"/><b class="caret"></b></a>

                                <ul class="scrollable-menu dropdown-menu ">
                                    <core:forEach var="category" items="${categories}">
                                        <li>
                                            <a href="${pageContext.request.contextPath}/main?cat=${category.id}">${category.name}</a>
                                        </li>
                                    </core:forEach>
                                </ul>
                            </li>

                            <li><a href="//www.linkedin.com/in/ragexe"><fmt:message key="navbar.footer.about"/></a></li>

                            <li><a href="mailto:ragexeone@gmail.com"><fmt:message key="navbar.footer.mail"/></a></li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="http://paullaros.bitbucket.org/material-blog/"><i class="fa fa-copyright"
                                                                                           aria-hidden="true"
                                                                                           title="CopyRight"></i></a>
                            </li>

                            <li><a href="//plus.google.com/u/0/105975638726038919806"><i class="fa fa-google-plus"
                                                                                         title="Google+"></i></a>
                            </li>

                            <li><a href="//bitbucket.org/ragexe/"><i class="fa fa-bitbucket" aria-hidden="true"
                                                                     title="BitBucket"></i></a></li>

                            <li>
                                <div class="form-group-sm">
                                    <form action="${pageContext.request.contextPath}/main" method="post">
                                        <input type="hidden" name="command" value="lang"/>

                                        <select class="form-control" id="language" name="language" onchange="this.form.submit()">
                                            <option value="ru" ${language == 'ru' ? 'selected' : ''}>Рус</option>

                                            <option value="en" ${language == 'en' ? 'selected' : ''}>Eng</option>

                                            <option value="be" ${language == 'be' ? 'selected' : ''}>Бел</option>
                                        </select>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <!-- Log In Section -->
        <section class="text-center login-image" style="color: #f9f9f9;">
            <div class="container">
                <div class="row" style="padding-top: 3em; padding-bottom: 3em">
                    <div class="col-lg-6 vcenter" style="text-align: left;">
                        <h2 style="font-size: 64px">Zen Publisher</h2>
                        <p><fmt:message key="login.center.p"/></p>
                    </div>

                    <div style="background: rgba(255, 255, 255, 0.155);" class="col-lg-4 vcenter">
                        <h2><fmt:message key="login.label.auth" /></h2>
                        <form method="post" action="j_security_check" >
                            <div class="form-group">
                                <input type="text" placeholder="<fmt:message key="login.label.username"/>" name="j_username" class="form-control">
                            </div>
                            <span class="material-input"></span>

                            <div class="form-group">
                                <input type="password" placeholder="<fmt:message key="login.label.password"/>" name="j_password" class="form-control">
                                <span class="help-block"></span>
                            </div>

                            <core:if test="${not empty param.fail}">
                                <div class="alert alert-dismissible alert-warning fade in">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <ftm:message key="login.alert.error"/>
                                </div>
                            </core:if>

                            <div class="form-group form-sidebar wrap" style="margin-bottom: 20px;">
                                <%-- TODO Forgot pass --%>
                                <div class="pull-right">
                                    <a href="#" style="font-size: 10px;"><fmt:message key="login.link.forgotpass"/></a>
                                </div>

                                <%-- TODO Remember me --%>
                                <div class="" style="text-align: left; float: none; position: relative; top: 4px; max-width: 200px;">
                                    <label style="margin-left: 3px;">
                                        <input type="checkbox" name="remember" style="float: left; margin-top: -1px;">
                                    </label>
                                    <p style="color: #696969; font-size: 10px; float: left"><fmt:message key="login.checkbox.rememberme"/> </p>
                                </div>
                            </div>

                            <div class="btn-group" style="margin-bottom: 17px;">
                                <input class="btn btn-info btn-raised btn-lg btn-block" type="submit" value="<fmt:message key="login.sidebar.button.signin"/>">
                                <a class="btn btn-lg btn-default btn-block" href="${pageContext.request.contextPath}/main?command=regpost"><fmt:message key="login.sidebar.button.singup"/></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <!-- About Section -->
        <section id="about" class="container content-section text-center" style="padding: 10em 0 6.85em 0; text-align: left;">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h1><ftm:message key="login.text.about"/></h1>
                    <p><ftm:message key="login.text.about.p1"/></p>
                    <p><ftm:message key="login.text.about.p2"/></p>
                    <p><ftm:message key="login.text.about.p3"/></p>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>
    </body>
</html>