<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fnt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>zenPublisher</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>

    </head>

    <body>
        <!-- Nav Bar -->
        <jsp:include page="common/navbar.jsp"/>

        <!-- Content -->
        <div class="container blog-content">
            <div class="row">
                <div class="col-md-12 blog-main">
                    <section class="blog-post">
                        <div class="panel panel-default">
                            <div class="panel-body" style="height: 80%;">
                                <div class="blog-post-meta">
                                    <div>
                                        <h3><fnt:message key="reg.form.header"/></h3>
                                    </div>
                                </div>

                                <div class="blog-post-content">
                                    <form id="userform" name="registration" class="form-horizontal style-form" method="post"
                                          action="${pageContext.request.contextPath}/main">
                                        <input type="hidden" name="command" value="signup"/>

                                        <%-- Image input --%>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><fmt:message key="quest.form.picture"/></label>

                                            <div class="col-md-10">
                                                <div class="input-group image-preview">
                                                    <input type="text" class="form-control image-preview-filename"
                                                           disabled="disabled"/>

                                                    <div class="input-group-btn">
                                                        <!-- image-preview-clear button -->
                                                        <button type="button" class="btn btn-default btn-sm image-preview-clear" style="display:none;">
                                                            <i class="fa fa-times" aria-hidden="true"></i><fmt:message key="comment.form.button.clear"/>
                                                        </button>

                                                        <!-- image-preview-input -->
                                                        <div class="btn btn-default btn-sm image-preview-input">
                                                            <i class="fa fa-folder-open" aria-hidden="true"></i>
                                                            <span class="image-preview-input-title"><fmt:message key="quest.form.button.choose"/></span>
                                                            <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <%-- Username --%>
                                        <div class="form-group">
                                            <label for="user-nickname" class="col-md-2 control-label"><fmt:message key="login.label.username"/></label>

                                            <div class="col-md-10">
                                                <input id="user-nickname" type="text" name="user-nickname" class="form-control">
                                            </div>
                                        </div>

                                        <%-- User password --%>
                                        <div class="form-group">
                                            <label for="user-password" class="col-md-2 control-label"><fmt:message key="login.label.password"/></label>

                                            <div class="col-md-10">
                                                <input id="user-password" type="password" name="user-password" class="form-control">
                                            </div>
                                        </div>

                                        <div id="form-group-user-password-repeat" class="form-group">
                                            <label for="user-password-repeat" class="col-md-2 control-label"><fmt:message key="login.label.password_repeat"/></label>

                                            <div class="col-md-10">
                                                <input id="user-password-repeat" type="password" name="user-password-repeat" class="form-control">

                                                <span id="help-block-password-repeat" class="help-block"><%-- func generate text --%></span>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>

                                            <div class="col-md-10">
                                                <core:if test="${not empty keyMessage}">
                                                    <div class="alert alert-dismissible alert-warning fade in">
                                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                                        <fmt:message key="${keyMessage}"/>
                                                    </div>
                                                </core:if>
                                            </div>
                                        </div>
                                    </form>

                                    <%--<form action="" name="registration">--%>

                                        <%--<label for="firstname">First Name</label>--%>
                                        <%--<input type="text" name="firstname" id="firstname" placeholder="John">--%>

                                        <%--<label for="lastname">Last Name</label>--%>
                                        <%--<input type="text" name="lastname" id="lastname" placeholder="Doe">--%>

                                        <%--<label for="email">Email</label>--%>
                                        <%--<input type="email" name="email" id="email" placeholder="john@doe.com">--%>

                                        <%--<label for="password">Password</label>--%>
                                        <%--<input type="password" name="password" id="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">--%>

                                        <%--<button type="submit">Register</button>--%>
                                    <%--</form>--%>
                                </div>
                            </div>

                            <div class="panel-footer">
                                <div class="btn-block">
                                    <input type="submit" form="userform" class="btn btn-raised btn-success" value="<fmt:message key="login.sidebar.button.singup"/>"/>
                                    <button type="reset" form="userform" class="btn btn-default"><fmt:message key="comment.form.button.clear"/></button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <%--<script src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>--%>
        <script src="${pageContext.request.contextPath}/libs/js/jquery.validate.min.js" ></script>

        <!-- Pic upload -->
        <script src="${pageContext.request.contextPath}/libs/js/pic_upload.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>


        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>

        <script>
            var lang = '${language}';
            jQuery.i18n.properties({
                name:'messages',
                path:'libs/bundle/',
                mode:'both',
                language: lang
//                async: false
            });
        </script>

        <script>
            $(document).ready(function() {
                $.validator.addMethod("regexNick", function(value, element, regexpr) {
                    return regexpr.test(value);
                }, jQuery.i18n.prop('signup.nick_inv'));

                $.validator.addMethod("regexPass", function(value, element, regexpr) {
                    return regexpr.test(value);
                }, jQuery.i18n.prop('signup.pass_inv'));

                $("form[name='registration']").validate({
                    rules: {
                        'user-nickname': {
                            required : true,
                            regexNick : /[_A-Za-z0-9-]{2,15}$/
                        },
                        'user-password': {
                            required : true,
                            regexPass : /[_A-Za-z0-9-]{2,15}$/
                        },
                        'user-password-repeat': {
                            required : true,
                            equalTo : "#user-password"
                        }

                    },
                    // Specify validation error messages
                    messages: {
                        'user-nickname': {
                            required : jQuery.i18n.prop('signup.nickname.empty')
                        },
                        'user-password': {
                            required : jQuery.i18n.prop('signup.pass.empty')
                        },
                        'user-password-repeat': {
                            required : jQuery.i18n.prop('signup.passrepeat.empty'),
                            equalTo : jQuery.i18n.prop('signup.pass_match')
                        }
                    },

                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        </script>
    </body>
</html>
