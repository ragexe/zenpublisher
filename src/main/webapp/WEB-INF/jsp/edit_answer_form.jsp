<%--<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--<jsp:useBean id="answer" class="by.training.entity.Answer" beanName="answer"></jsp:useBean>--%>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <title>zenPublisher</title>

        <%-- Animation js, styles, fonts --%>
        <jsp:include page="common/style.jsp"/>
    </head>

    <body>
        <!-- Nav Bar -->
        <jsp:include page="common/navbar.jsp"/>

        <!-- Content  -->
        <div class="container blog-content">
            <div class="row">
                <!-- Sidebar -->
                <jsp:include page="../jsp/common/sidebar.jsp"/>

                <div class="col-md-8 col-md-offset-1 blog-main">
                    <div class="row">
                        <div class="col-sm-12">
                            <section class="blog-post">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="blog-post-content">
                                            <h2 class="blog-post-title"><fmt:message key="answer.edit.form.title"/></h2>

                                            <!-- FORM - Edit answer -->
                                            <form id="postform" class="form-horizontal style-form" method="post"
                                                  action="${pageContext.request.contextPath}/auth">
                                                <input type="hidden" name="command" value="UPDATE_A"/>

                                                <input type="hidden" name="id" value="${question.id}"/>

                                                <input type="hidden" name="user" value="${pageContext.request.remoteUser}"/>

                                                <%-- Date of publication --%>
                                                <div class="form-group">
                                                    <label for="dateform" class="col-md-2 col-md-2 control-label"><fmt:message key="quest.edit.form.label.date"/></label>

                                                    <div class="col-md-10">
                                                        <input disabled id="dateform" type="datetime" class="form-control" value="${answer.datetime_crtn}">
                                                    </div>
                                                </div>

                                                <%-- Answer body --%>
                                                <div class="form-group">
                                                    <label for="main" class="col-md-2 col-md-2 control-label"><fmt:message key="quest.form.label.quest_body"/></label>

                                                    <div class="col-md-10">
                                                        <textarea id="main" name="body" class="form-control limitInput" rows="6"
                                                              placeholder="<fmt:message key="quest.form.textarea.quest_body"/>">${answer.body}</textarea>

                                                        <span id="help-block-main"><!-- func validation --></span>
                                                    </div>
                                                </div>

                                                <%-- Answer author --%>
                                                <div class="form-group">
                                                    <label for="dateform" class="col-md-2 col-md-2 control-label"><fmt:message key="quest.edit.form.label.sign"/></label>

                                                    <div class="col-md-10">
                                                        <input disabled id="author" type="text" class="form-control" value="${answer.usr_nickname}">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="btn-group">
                                            <input type="submit" disabled id="postform-submit-main" form="postform" value="<fmt:message key="quest.form.button.send"/>"
                                                   class="btn btn-info btn-lg"/>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <jsp:include page="common/footer.jsp"/>

        <!-- Top scroll button -->
        <button class="material-scrolltop primary" type="button"></button>

        <script src="${pageContext.request.contextPath}/libs/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/ripples.min.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/material.min.js"></script>

        <script src="${pageContext.request.contextPath}/libs/js/material-scrolltop.js"></script>
        <script src="${pageContext.request.contextPath}/libs/js/main.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/libs/js/jquery.timeago.${language}.js"></script>

        <script type="text/javascript" language="JavaScript"
                src="${pageContext.request.contextPath}/libs/js/jquery.i18n.properties.min.js"></script>

        <%-- TimeAgo --%>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery("time.timeago").timeago();
            });
        </script>

        <%-- Scroll Top Button --%>
        <script>
            $.material.init();
            $('body').materialScrollTop();
        </script>

        <%-- i18n --%>
        <script>
            var lang = '${language}';
            jQuery.i18n.properties({
                name:'messages',
                path:'libs/bundle/',
                mode:'both',
                language: lang,
                async: true
            });
        </script>

        <%-- Validation Textareas --%>
        <script>
            var max = 255;
            $(document).ready(function () {
                $('.limitInput').keyup(function () {
                    var id = this.id;
                    var count = $(this).val().length;
                    var num = max - count;
                    if ($(this).val().length > 0) {
                        if (num >= 0) {
                            $('#help-block-' + id).text(jQuery.i18n.prop('validation.symbolsLeft')+ ' ' + num).css("color", "");
                            $('#postform-submit-' + id).removeAttr("disabled");
                        } else {
                            $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.achieveLimit') + ' (' + (-num) + ')');
                            $('#postform-submit-' + id).attr("disabled", "true");
                        }
                    } else {
                        $('#help-block-' + id).css("color", "#fb8c84").text(jQuery.i18n.prop('validation.noSymbols'));
                        $('#postform-submit-' + id).attr("disabled", "true");
                    }
                });
            });
        </script>

        <%-- Pic upload --%>
        <script src="${pageContext.request.contextPath}/libs/js/pic_upload.js"></script>
    </body>
</html>
