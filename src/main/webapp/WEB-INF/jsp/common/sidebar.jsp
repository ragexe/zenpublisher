<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<div class="col-md-3 blog-sidebar" id="sidebar">
    <!-- Search sidebar module -->
    <div class="sidebar-module">
        <div class="form-group label-placeholder is-empty">
            <label for="i5p" class="control-label"><i class="fa fa-search" aria-hidden="true"></i><span> <fmt:message key="sidebar.search"/></span></label>
            <input type="text" class="form-control" id="i5p">
        </div>
    </div>

    <core:choose>
        <%-- If user is NOT authenticated --%>
        <core:when test="${empty pageContext.request.remoteUser}">
            <!-- Unauthenticated user panel sidebar module -->
            <div class="sidebar-module">
                <div class="panel">
                    <!--Auth/Sign-In panel-->
                    <div class="panel-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="${pageContext.request.contextPath}/main?command=regpost" class="btn btn-raised"><fmt:message key="login.sidebar.button.singup"/></a>
                            <a href="${pageContext.request.contextPath}/auth" class="btn btn-raised btn-info"><fmt:message key="login.sidebar.button.signin"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </core:when>

        <%-- If user is authenticated --%>
        <core:otherwise>
            <!--Authenticated user panel sidebar module-->
            <div class="sidebar-module">
                <div class="panel panel-default">
                    <core:if test="${pageContext.request.isUserInRole('admin')}">
                        <img src="${pageContext.request.contextPath}/icons/admin.png" style="position: absolute" alt="admin label">
                    </core:if>

                    <!--Background picture-->
                    <div class="panel-heading userAvaBackground userAvaImage">

                        <!--Avatar-->
                        <img alt="user's avatar" src="${pageContext.request.contextPath}/img/image-4.jpg">

                        <!--Nickname-->
                        <h4 id="user-nickname-sidebar">
                            <a href="#">${pageContext.request.remoteUser}</a>
                            <%--<span class="label label-light label-primary pull-right">8.123</span>--%>
                            <span class="label label-light label-primary pull-right">${pageContext.request.userPrincipal.user.averageFame}
                                <a href="#" style="color: initial;"><i class="fa fa-refresh" aria-hidden="true"></i></a></span>
                        </h4>
                    </div>
                    
                    <!--User panel-->
                    <div class="panel-body">
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-question-circle-o" aria-hidden="true"></i><a href="#"><fmt:message key="sidebar.questions"/></a>
                                <a href="#" class="label-sm label-light label-default">5</a></li>

                            <li><i class="fa-li fa fa-comments-o" aria-hidden="true"></i><a href="#"><fmt:message key="sidebar.answers"/></a>
                                <a href="#" class="label-sm label-light label-default">10</a></li>

                            <li><i class="fa-li fa fa-bookmark-o" aria-hidden="true"></i><a href="#"><fmt:message key="sidebar.marks"/></a></li>

                            <li><i class="fa-li fa fa-times" aria-hidden="true"></i><a href="${pageContext.request.contextPath}/auth?command=logout"><fmt:message key="sidebar.logout"/></a></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <!--Button "Спросить"-->
                        <a class="btn btn-primary btn-raised btn-block btn-lg"
                           href="${pageContext.request.contextPath}/auth?command=new"
                           target="_blank"><fmt:message key="sidebar.new_question"/></a>
                    </div>
                </div>
            </div>
        </core:otherwise>
    </core:choose>

    <!-- Archive sidebar module -->
    <div class="sidebar-module">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4><fmt:message key="sidebar.archieve"/></h4>
                <ol class="list-unstyled">
                    <%-- TODO locale bundle --%>
                    <li><a href="#">Февраль 2016</a></li>
                    <li><a href="#">Январь 2016</a></li>
                    <li><a href="#">Декабрь 2015</a></li>
                    <li><a href="#">Ноябрь 2015</a></li>
                    <li><a href="#">Октябрь 2015</a></li>
                    <li><a href="#">Сентябрь 2015</a></li>
                    <li><a href="#">Август 2015</a></li>
                    <li><a href="#">Июль 2015</a></li>
                    <li><a href="#">Июнь 2015</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>

