<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%-- Locale --%>
<core:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale.language}" scope="session" />

<%-- Context path --%>
<core:set scope="request" value="${pageContext.request.contextPath}" var="contextPath"/>

<%-- Favicon --%>
<link rel="icon" type="image/gif" href="${contextPath}/icons/card.gif" />
<link rel="shortcut icon" type="image/gif" href="${contextPath}/icons/card.gif" />

<!-- Loading animation -->
<script src="${contextPath}/libs/js/pace.min.js"></script>

<!-- Bootstrap core CSS and Material Bootstrap -->
<link href="${contextPath}/libs/css/bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="${contextPath}/libs/css/bootstrap-material-design.css" rel="stylesheet">
<link href="${contextPath}/libs/css/ripples.min.css" rel="stylesheet">
<link href="${contextPath}/libs/css/material-scrolltop.css" rel="stylesheet">
<link href="${contextPath}/libs/css/snackbar.min.css" rel="stylesheet"/>

<!-- Custom styles for this template -->
<link href="${contextPath}/libs/css/material-blog.css" rel="stylesheet">

<!-- Fonts -->
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="${contextPath}/libs/js/html5shiv.min.js"></script>
<script src="${contextPath}/libs/js/respond.min.js"></script>
<![endif]-->

