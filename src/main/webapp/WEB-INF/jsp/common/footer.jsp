<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<footer class="blog-footer">
    <div id="links">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <a href="//fezvrasta.github.io/bootstrap-material-design/"><i
                            class="material-icons brand"></i></a>
                </div>

                <div class="col-md-8 col-sm-8 text-center offset">
                    <ul class="list-inline">
                        <li><a href="${pageContext.request.contextPath}/main"><i class="fa fa-home" aria-hidden="true"></i> <fmt:message key="navbar.footer.home"/></a></li>
                        <li><a href="//www.linkedin.com/in/ragexe"><i class="fa fa-linkedin-square" aria-hidden="true"></i> <fmt:message key="navbar.footer.about"/></a></li>
                        <li><a href="mailto:ragexeone@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> <fmt:message key="navbar.footer.mail"/></a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-2 text-right offset">
                    <img alt="logo" src="${pageContext.request.contextPath}/icons/ragexe-logo.png" class="footer-logo">
                </div>
            </div>
        </div>
    </div>
</footer>

