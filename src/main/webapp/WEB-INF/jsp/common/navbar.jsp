<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- Locale --%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="text" />

<header>
    <div class="navbar navbar-material-blog navbar-primary navbar-absolute-top">
        <div class="navbar-image"></div>

        <div class="navbar-wrapper container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="${pageContext.request.contextPath}/main" title="<fmt:message key="navbar.footer.home"/>">
                    <img class="main-icon" src="${pageContext.request.contextPath}/icons/zenIconInv.png"
                         alt="enso icon">

                    <span>zenPublisher</span>
                </a>
            </div>

            <div class="navbar-collapse collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <li class="active dropdown">
                        <a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navbar.categories"/><b
                                    class="caret"></b></a>
                        <ul class="scrollable-menu dropdown-menu ">
                            <core:forEach var="category" items="${categories}">
                                <li>
                                    <a href="${pageContext.request.contextPath}/main?cat=${category.id}">${category.name}</a>
                                </li>
                            </core:forEach>
                            <li>
                                <a href="${pageContext.request.contextPath}/main"><strong>Все категории</strong></a>
                            </li>
                        </ul>
                    </li>

                    <li><a href="//www.linkedin.com/in/ragexe"><i class="fa fa-linkedin-square" aria-hidden="true"></i> <fmt:message key="navbar.footer.about"/></a></li>

                    <li><a href="mailto:ragexeone@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> <fmt:message key="navbar.footer.mail"/></a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="http://paullaros.bitbucket.org/material-blog/"><i class="fa fa-copyright"
                                                                                   aria-hidden="true"
                                                                                   title="CopyRight"></i></a>
                    </li>

                    <li><a href="//plus.google.com/u/0/105975638726038919806"><i class="fa fa-google-plus"
                                                                                 title="Google+"></i></a>
                    </li>

                    <li><a href="//bitbucket.org/ragexe/"><i class="fa fa-bitbucket" aria-hidden="true"
                                                             title="BitBucket"></i></a></li>

                    <li>
                        <div class="form-group-sm">
                            <form action="${pageContext.request.contextPath}/main" method="post">
                                <input type="hidden" name="command" value="lang"/>

                                <select class="form-control" id="language" name="language" onchange="this.form.submit()">
                                    <option value="ru" ${language == 'ru' ? 'selected' : ''}>Рус</option>

                                    <option value="en" ${language == 'en' ? 'selected' : ''}>Eng</option>

                                    <option value="be" ${language == 'be' ? 'selected' : ''}>Бел</option>
                                </select>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
