package by.training.command.auth;

import by.training.command.ICommand;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import by.training.validation.exception.ValidatorException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

class EstimateQuestCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(EstimateQuestCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        IValidator validator = new ValidatorImpl();

        String user = request.getRemoteUser();
        int questId;
        short mark;
        Timestamp time = new Timestamp(new Date().getTime() + SYNC_TIME);

        try {
            questId = Integer.parseInt(request.getParameter("quest"));
            mark = (short) Integer.parseInt(request.getParameter("mrk"));

            if(validator.isValidQuestionMark(service, questId, mark)){
                service.updateQuestMark(user, questId, mark, time);
            }

            response.sendRedirect(request.getHeader("referer") == null ? request.getContextPath().concat("/main?command=quest&id=").concat(String.valueOf(questId)) : request.getHeader("referer"));
        } catch (ServiceException | IOException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        } catch (NumberFormatException | ValidatorException e) {
            logger.debug(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
