package by.training.command.auth;

import by.training.command.ICommand;
import by.training.command.ICommandEnum;

public enum AuthorizedCommandCommandEnum implements ICommandEnum {
    LOGOUT(new LogoutCommand()),
//    SHOWALL(new ShowQuestionsCommand()),
//    QUEST(new ShowDetailQuestionCommand()),
    ESTQ(new EstimateQuestCommand()),
    ESTA(new EstimateAnswerCommand()),
    NEW(new FormQuestCommand()),
    POST(new PostQuestCommand()),
    ANS(new PostAnswerCommand()),
    EDITQ(new FormEditQuestCommand()),
    UPDATEQ(new UpdateQuestCommand()),
    UPDATE_A(new UpdateAnswerCommand()),
    EDITA(new FormEditAnswerCommand()),
    DELQ(new DeleteQuestCommand()),
    DELA(new DeleteAnswerCommand());

    private ICommand command;

    public ICommand getCurrentCommand() {
        return command;
    }

    AuthorizedCommandCommandEnum(ICommand command) {
        this.command = command;
    }
}

