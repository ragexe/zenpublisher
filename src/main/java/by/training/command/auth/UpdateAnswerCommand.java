package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Answer;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import by.training.validation.exception.ValidatorException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

class UpdateAnswerCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(UpdateAnswerCommand.class);
    private String page;



    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        IValidator validator = new ValidatorImpl();

        Answer answer;

        try {
            answer = (Answer) request.getSession().getAttribute("answer");
            answer.setBody(request.getParameter("body"));
            answer.setDatetime_upd(new Timestamp(new Date().getTime() + SYNC_TIME));

            validator.isValidEditedAnswerToUpdate(service, answer);

            service.updateAnswer(answer);

            request.getSession().removeAttribute("answer");

            response.sendRedirect(request.getContextPath() +  "/main?command=quest&id=" + answer.getQuest_id());
        } catch (ServiceException | IOException | NumberFormatException | ValidatorException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
