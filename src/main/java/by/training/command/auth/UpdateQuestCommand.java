package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Question;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.exception.ServiceException;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import by.training.validation.exception.ValidatorException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import static by.training.command.basic.BasicCommandCommandEnum.QUEST;

class UpdateQuestCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(UpdateQuestCommand.class);
    private String page;



    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        IValidator validator = new ValidatorImpl();

        Question question;

        try {
            question = (Question) request.getSession().getAttribute("question");
            question.setCategr_id(Integer.parseInt(request.getParameter("category")));
            question.setTitle(request.getParameter("title"));
            question.setBody(request.getParameter("body"));
            question.setDate_upd(new Timestamp(new Date().getTime() + SYNC_TIME));

            if (validator.isValidQuestionToSave(service, question)){
                service.updateQuestion(question);
                request.getSession().removeAttribute("question");
            }

            response.sendRedirect(request.getContextPath() +  "/main?command=" + QUEST.toString().toLowerCase() + "&id=" + question.getId());
        } catch (ServiceException | IOException | NumberFormatException | ValidatorException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
