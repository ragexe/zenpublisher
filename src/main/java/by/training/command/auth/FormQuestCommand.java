package by.training.command.auth;

import by.training.command.ICommand;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class FormQuestCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(FormQuestCommand.class);
    private String page = PathHandler.getProperty("path.page.error");

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty("path.page.questform");

        return page;
    }
}
