package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Question;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class DeleteQuestCommand implements ICommand {
    private String page;
    private static final Logger logger = Logger.getLogger(DeleteQuestCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty("path.page.error");
        IService service = new AppServiceImpl();
        int id;

        try {
            id = Integer.parseInt(request.getParameter("id"));
            Question question = service.getQuestionById(id);

            if (question == null){
                throw new CommandException("Bad request");
            }

            if (!question.getUsr_nickname().equals(request.getRemoteUser()) && !request.isUserInRole("admin")){
                throw new CommandException("Access denied");
            }

            service.deleteQuestion(id);

            response.sendRedirect(request.getContextPath() + "/main");

        } catch (IOException | ServiceException e) {
            logger.error(e);
            request.setAttribute("exception", e);
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return page;
        } catch (CommandException | NumberFormatException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
