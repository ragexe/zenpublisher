package by.training.command.auth;

import by.training.command.ICommand;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class LogoutCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(LogoutCommand.class);
    private String page = PathHandler.getProperty("path.page.error");

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        // Invalidate current HTTP session.
        // Will call JAAS LoginModule logout() method
        request.getSession().invalidate();

        try {
            response.sendRedirect(request.getContextPath() + "/main");
        } catch (IOException e) {
            logger.error(e);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
