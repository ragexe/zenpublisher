package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Answer;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.basic.BasicCommandCommandEnum.QUEST;

class DeleteAnswerCommand implements ICommand {
    private String page;
    private static final Logger logger = Logger.getLogger(DeleteAnswerCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty("path.page.error");
        IService service = new AppServiceImpl();
        int id;

        try {
            id = Integer.parseInt(request.getParameter("id"));
            Answer answer = service.getAnswerById(id);

            if (answer == null){
                throw new CommandException("Bad request");
            }

            if (!answer.getUsr_nickname().equals(request.getRemoteUser()) && !request.isUserInRole("admin")){
                throw new CommandException("Access denied");
            }

            service.deleteAnswer(id);

            response.sendRedirect(request.getContextPath() + "/main?command=" + QUEST.toString().toLowerCase() + "&id=" + answer.getQuest_id());
        } catch (IOException | ServiceException e) {
            logger.error(e);
            request.setAttribute("exception", e);
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return page;
        } catch (CommandException | NumberFormatException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            request.setAttribute("exception", e);
            return page;
        }
        return null;
    }
}
