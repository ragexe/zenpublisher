package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Answer;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class FormEditAnswerCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(FormEditAnswerCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();

        try {
            int id = Integer.parseInt(request.getParameter("id"));
            Answer answer = service.getAnswerById(id);

            if (answer == null){
                throw new CommandException("Bad request");
            }

            if (!answer.getUsr_nickname().equals(request.getRemoteUser()) && !request.isUserInRole("admin")){
                throw new CommandException("Access denied");
            }
            page = PathHandler.getProperty(PathHandler.PathEnum.EDIT_ANSWER_FORM);

            request.getSession().setAttribute("answer", answer);

        } catch (CommandException | ServiceException | NumberFormatException e) {
            logger.info(e);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            request.setAttribute("exception", e);
            return page;
        }

        return page;
    }
}
