package by.training.command.auth;

import by.training.command.ICommand;
import by.training.entity.Answer;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import by.training.validation.exception.ValidatorException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

class PostAnswerCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(PostAnswerCommand.class);
    private String page;
    private int idOfNewAnswer;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        IValidator validator = new ValidatorImpl();

        Answer answer = new Answer();

        try {

            answer.setQuest_id(Integer.parseInt(request.getParameter("questionId")));
            answer.setUsr_nickname(request.getRemoteUser());
            answer.setAnswr_id(Integer.parseInt(request.getParameter("parentCommentId")));
            answer.setBody(request.getParameter("body"));
            answer.setDatetime_crtn(new Timestamp(new Date().getTime() + SYNC_TIME));

            if (validator.isValidAnswerToSave(service, answer)){
                idOfNewAnswer = service.saveAnswer(answer);
            }

            response.sendRedirect(request.getHeader("Referer") + "#comment-" + idOfNewAnswer);
        } catch (IOException | ServiceException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        } catch (ValidatorException | NumberFormatException e){
            logger.debug(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        }
        return null;
    }
}
