package by.training.command;

import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class EmptyCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(EmptyCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);
        try {
            response.sendRedirect(request.getContextPath() + "/main");
        } catch (IOException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            request.setAttribute("exception", e);
            return page;
        }
        return null;
    }
}
