package by.training.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ICommand {
    //    +1 hour synchronization
    int SYNC_TIME = 3_600_000;

    String execute(HttpServletRequest request, HttpServletResponse response);


}



