package by.training.command;

public interface ICommandEnum {
    ICommand getCurrentCommand();
}
