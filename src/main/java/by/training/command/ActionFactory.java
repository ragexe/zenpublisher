package by.training.command;

import by.training.command.auth.AuthorizedCommandCommandEnum;
import by.training.command.basic.BasicCommandCommandEnum;
import by.training.command.basic.ShowQuestionsCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ActionFactory {
    private final static Logger logger = Logger.getLogger(ActionFactory.class);

    public ICommand defineCommand(HttpServletRequest request, boolean isSecured, HttpServletResponse response) throws IOException {
        ICommand current;
        ICommandEnum commandEnum;
        String action = request.getParameter("command");


        if (isSecured) {
            current = new EmptyCommand();
            if (action == null || action.isEmpty()) {
                return current;
            }
            commandEnum = AuthorizedCommandCommandEnum.valueOf(action.toUpperCase());
        } else {
            current = new ShowQuestionsCommand();
            if (action == null || action.isEmpty()) {
                return current;
            }
            commandEnum = BasicCommandCommandEnum.valueOf(action.toUpperCase());
        }
        current = commandEnum.getCurrentCommand();

        return current;
    }

    public ICommand defineCommand(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return defineCommand(request, false, response);
    }
}
