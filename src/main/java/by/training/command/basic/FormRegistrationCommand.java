package by.training.command.basic;

import by.training.command.ICommand;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

class FormRegistrationCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(FormRegistrationCommand.class);
    private String page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.SIGNUP_FORM);

        return page;
    }
}
