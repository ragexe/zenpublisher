package by.training.command.basic;

import by.training.command.ICommand;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class ChangeLangCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(ChangeLangCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IValidator validator = new ValidatorImpl();

        String language = request.getParameter("language");

        language = validator.validateLanguage(language);

        request.getSession().setAttribute("language", language);

        try {
            response.sendRedirect(request.getHeader("referer"));
        } catch (IOException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            request.setAttribute("exception", e);
            return page;
        }

        return null;
    }
}
