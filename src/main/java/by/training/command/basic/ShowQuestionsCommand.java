package by.training.command.basic;

import by.training.command.ICommand;
import by.training.entity.Question;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowQuestionsCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(ShowQuestionsCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        List<Question> questionList;

        int numberOfPage = 1;
        int recordsPerPage = 5;
        int quantityOfRecords;
        int quantityOfPages;

        int categoryId;

        try {
            if(request.getParameter("page") != null) {
                numberOfPage = Integer.parseInt(request.getParameter("page"));
                if (numberOfPage < 1){
                    throw new CommandException("Wrong page number");
                }
            }

            if (request.getParameter("cat") == null || request.getParameter("cat").isEmpty()) {
                try {
                    questionList = service.getAllQuestions(recordsPerPage, (numberOfPage-1)*recordsPerPage);
                } catch (ServiceException e) {
                    logger.error(e);
                    throw new CommandException(e);

                }
            } else {
                categoryId = Integer.parseInt(request.getParameter("cat"));
                try {
                    questionList = service.getAllQuestionsByCategrId(categoryId, recordsPerPage, (numberOfPage-1)*recordsPerPage);
                } catch (ServiceException e) {
                    logger.error(e);
                    throw new CommandException(e);
                }
                request.setAttribute("categoryId", categoryId);
            }

            quantityOfRecords = service.getQuantityOfRecords() < recordsPerPage ? recordsPerPage : service.getQuantityOfRecords();
            quantityOfPages = (int) Math.ceil(quantityOfRecords * 1.0 / recordsPerPage);

            if (numberOfPage > quantityOfPages && quantityOfPages != 0){
                throw new CommandException("Wrong page number");
            }
        } catch (CommandException e) {
            logger.error(e);
            request.setAttribute("exception", e);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return page;
        } catch (NumberFormatException e) {
            logger.error(e);
            request.setAttribute("exception", e);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return page;
        }

        request.setAttribute("quantityOfPages", quantityOfPages);
        request.setAttribute("currentPage", numberOfPage);
        request.setAttribute("questionList", questionList);

        page = PathHandler.getProperty(PathHandler.PathEnum.MAIN);
        return page;
    }
}
