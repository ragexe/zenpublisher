package by.training.command.basic;

import by.training.command.ICommand;
import by.training.entity.User;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.validation.exception.ValidatorBundledMessagesException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

class RegistrationCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(RegistrationCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty(PathHandler.PathEnum.ERROR_PAGE);

        IService service = new AppServiceImpl();
        IValidator validator = new ValidatorImpl();

        String nickname = request.getParameter("user-nickname");
        String password = request.getParameter("user-password");
        String passwordR = request.getParameter("user-password-repeat");

        boolean dataIsValid;

        try {
            dataIsValid = validator.isValidNewUserData(service, nickname, password, passwordR);

            if (dataIsValid) {
                User user = new User();
                user.setRole(User.EnumUserRole.COMMON);
                user.setDate_crtn(new Date(new java.util.Date().getTime() + SYNC_TIME));
                user.setNickname(nickname);
                user.setPassword(password);

                service.saveUser(user);

                response.sendRedirect(request.getContextPath() + "/auth");
            }

        } catch (ServiceException | IOException e) {
            logger.error(e);
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            request.setAttribute("exception", new CommandException(e));
            return page;
        } catch (ValidatorBundledMessagesException e) {
            request.setAttribute("keyMessage", e.getKeyOfMessage());
            return (new FormRegistrationCommand()).execute(request, response);
        }

        return null;
    }
}
