package by.training.command.basic;

import by.training.command.ICommand;
import by.training.command.ICommandEnum;

public enum BasicCommandCommandEnum implements ICommandEnum {
    SHOWALL(new ShowQuestionsCommand()),
    REGPOST(new FormRegistrationCommand()),
    SIGNUP(new RegistrationCommand()),
    QUEST(new ShowDetailQuestionCommand()),
    LANG(new ChangeLangCommand());

    private ICommand command;

    public ICommand getCurrentCommand() {
        return command;
    }

    BasicCommandCommandEnum(ICommand command) {
        this.command = command;
    }
}

