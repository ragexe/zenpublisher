package by.training.command.basic;

import by.training.command.ICommand;
import by.training.dto.AnswerDTO;
import by.training.entity.Question;
import by.training.command.exception.CommandException;
import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowDetailQuestionCommand implements ICommand {
    private static final Logger logger = Logger.getLogger(ShowDetailQuestionCommand.class);
    private String page;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        page = PathHandler.getProperty("path.page.error");
        IService service = new AppServiceImpl();
        Question question;
        List<AnswerDTO> answers;

        String id = request.getParameter("id");

        if (id != null && !id.isEmpty()) {
            try {
                question = service.getQuestionById(Integer.parseInt(request.getParameter("id")));
                if (question == null){
                    throw new CommandException("No questions with such id");
                }

                answers = service.getAnswersByQuestId(Integer.parseInt(id));
                request.setAttribute("question", question);
                request.setAttribute("answers", answers);
            } catch (ServiceException | CommandException | NumberFormatException e) {
                logger.error(e);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                request.setAttribute("exception", e);
                return page;
            }
        }
        page = PathHandler.getProperty("path.page.viewQuestion");
        return page;
    }
}
