package by.training.servlet;

import by.training.service.exception.ServiceException;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import by.training.service.util.PathHandler;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener("application context listener")
public class ContextListener implements ServletContextListener {

    /**
     * Initialize log4j when the application is being started
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        // init log4j
        ServletContext context = event.getServletContext();
//        String log4jConfigFile = context.getInitParameter("log4j-config-location");
//        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
        String log4jConfigFile = context.getInitParameter("resources-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile + "log4j.properties";

        PropertyConfigurator.configure(fullPath);
        Logger logger = Logger.getLogger(ContextListener.class);
        logger.debug("log4j has been Initialized. Config path " + fullPath);
        logger.debug("Contex has been Initialized. Contex path " + context.getRealPath(""));

        // init PathHandler
//        String pathConfigFile = context.getInitParameter("path-config-location");
//        String pathConfigFile = context.getInitParameter("resources-path");

//        String fullPathConfigFile = context.getRealPath("") + File.separator + pathConfigFile;
//        PathHandler.setPathToProps(fullPathConfigFile);
        PathHandler.setPathToRes(context.getRealPath("").concat(File.separator).concat(context.getInitParameter("resources-location")));
//        ConfigurationManager.setPathToRes(context.getRealPath("").concat(File.separator).concat(context.getInitParameter("resources-location")));

        initCategories(context);

    }


    private static void initCategories(ServletContext context){
        IService service = new AppServiceImpl();
        Logger logger = Logger.getLogger(ContextListener.class);
        try {
            context.setAttribute("categories", service.getAllCategories());
        } catch (ServiceException e) {
            logger.error(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // do nothing

    }
}
