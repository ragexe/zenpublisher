package by.training.security.principal;

import by.training.entity.User;
import java.security.Principal;

public class UserPrincipal implements Principal {

    private String name;
    private User user;

    public UserPrincipal(String name) {
        super();
        this.name = name;
    }

    public UserPrincipal(String name, User user) {
        this(name);
        this.user = user;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}