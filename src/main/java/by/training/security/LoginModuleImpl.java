package by.training.security;

import by.training.entity.User;
import by.training.service.exception.ServiceException;
import by.training.security.principal.RolePrincipal;
import by.training.security.principal.UserPrincipal;
import by.training.service.AppServiceImpl;
import by.training.service.IService;
import org.apache.log4j.Logger;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LoginModuleImpl implements LoginModule {
	private static final Logger logger = Logger.getLogger(LoginModuleImpl.class);

	private CallbackHandler handler;
	private Subject subject;
	private UserPrincipal userPrincipal;
	private RolePrincipal rolePrincipal;
	private String login;
	private List<String> userGroups;

    private User user;

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
		handler = callbackHandler;
		this.subject = subject;
	}

	@Override
	public boolean login() throws LoginException {

		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("login");
		callbacks[1] = new PasswordCallback("password", false);

		try {
			handler.handle(callbacks);
			String name = ((NameCallback) callbacks[0]).getName();
			String password = String.valueOf(((PasswordCallback) callbacks[1]).getPassword());


			IService service = new AppServiceImpl();
//			User user = null;

			try {
				user = service.getUser(name, password);
				user.setAverageFame(service.getUserFame(user.getNickname()));
			} catch (ServiceException e) {
				logger.error(e);
				throw new LoginException(e.getMessage());
			}


			if (user.isValid()) {
				userGroups = new ArrayList<>();
				String role = user.getRole().toString().toLowerCase();
				userGroups.add(role);
				return true;
			}

            user = null;
			// If credentials are NOT OK we throw a LoginException

			throw new LoginException("Authentication failed");


		} catch (IOException | UnsupportedCallbackException e) {
			logger.error(e);
			throw new LoginException(e.getMessage());
		}

	}

	@Override
	public boolean commit() throws LoginException {
//		userPrincipal = new UserPrincipal(login);
        userPrincipal = new UserPrincipal(login, user);
		subject.getPrincipals().add(userPrincipal);

		if (userGroups != null && userGroups.size() > 0) {
			for (String groupName : userGroups) {
				rolePrincipal = new RolePrincipal(groupName);
				subject.getPrincipals().add(rolePrincipal);
			}
		}
		return true;
	}

	@Override
	public boolean abort() throws LoginException {
		return false;
	}

	@Override
	public boolean logout() throws LoginException {
		subject.getPrincipals().remove(userPrincipal);
		subject.getPrincipals().remove(rolePrincipal);
		return true;
	}

}
