package by.training.security;

import javax.security.auth.callback.*;
import java.io.IOException;

public class CallbackHandlerImpl implements CallbackHandler {
    private String user = null;
    private String password = null;

    public CallbackHandlerImpl(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        int counter = 0;
        while(counter < callbacks.length){
            if (callbacks[counter] instanceof NameCallback){
                NameCallback nameCallback = (NameCallback) callbacks[counter++];
                nameCallback.setName(user);
            } else if (callbacks[counter] instanceof PasswordCallback){
                PasswordCallback passwordCallback = (PasswordCallback) callbacks[counter++];
                passwordCallback.setPassword(password.toCharArray());
            }
        }
    }
}
