package by.training.entity;

import by.training.entity.common.EnumEntityStatus;

import java.sql.Timestamp;

public class Answer {
    protected int id;
    protected String usr_nickname;
    protected int answr_id;
    protected int quest_id;
    protected Timestamp datetime_crtn;
    protected String body;
    protected Timestamp datetime_upd;
    protected EnumEntityStatus status;
    protected float averageMark;

    public Answer() {
    }

//    for testing
    public Answer(int id, int answr_id) {
        this.id = id;
        this.answr_id = answr_id;
    }

    public Answer(int id, String usr_nickname, int answr_id, int quest_id, Timestamp datetime_crtn, String body, Timestamp datetime_upd, EnumEntityStatus status, float averageMark) {
        this.id = id;
        this.usr_nickname = usr_nickname;
        this.answr_id = answr_id;
        this.quest_id = quest_id;
        this.datetime_crtn = datetime_crtn;
        this.body = body;
        this.datetime_upd = datetime_upd;
        this.status = status;
        this.averageMark = averageMark;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getUsr_nickname() {
        return usr_nickname;
    }
    public void setUsr_nickname(String usr_nickname) {
        this.usr_nickname = usr_nickname;
    }

    public int getAnswr_id() {
        return answr_id;
    }
    public void setAnswr_id(int answr_id) {
        this.answr_id = answr_id;
    }

    public int getQuest_id() {
        return quest_id;
    }
    public void setQuest_id(int quest_id) {
        this.quest_id = quest_id;
    }

    public Timestamp getDatetime_crtn() {
        return datetime_crtn;
    }
    public void setDatetime_crtn(Timestamp datetime_crtn) {
        this.datetime_crtn = datetime_crtn;
    }

    public String getBody() {
        return body;
    }
    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getDatetime_upd() {
        return datetime_upd;
    }
    public void setDatetime_upd(Timestamp datetime_upd) {
        this.datetime_upd = datetime_upd;
    }

    public EnumEntityStatus getStatus() {
        return status;
    }
    public void setStatus(EnumEntityStatus status) {
        this.status = status;
    }

    public float getAverageMark() {
        return averageMark;
    }
    public void setAverageMark(float averageMark) {
        this.averageMark = averageMark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Answer)) return false;

        Answer answer = (Answer) o;

        if (id != answer.id) return false;
        if (answr_id != answer.answr_id) return false;
        if (quest_id != answer.quest_id) return false;
        if (Float.compare(answer.averageMark, averageMark) != 0) return false;
        if (usr_nickname != null ? !usr_nickname.equals(answer.usr_nickname) : answer.usr_nickname != null)
            return false;
        if (datetime_crtn != null ? !datetime_crtn.equals(answer.datetime_crtn) : answer.datetime_crtn != null)
            return false;
        if (body != null ? !body.equals(answer.body) : answer.body != null) return false;
        if (datetime_upd != null ? !datetime_upd.equals(answer.datetime_upd) : answer.datetime_upd != null)
            return false;
        return status == answer.status;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (usr_nickname != null ? usr_nickname.hashCode() : 0);
        result = 31 * result + answr_id;
        result = 31 * result + quest_id;
        result = 31 * result + (datetime_crtn != null ? datetime_crtn.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (datetime_upd != null ? datetime_upd.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (averageMark != +0.0f ? Float.floatToIntBits(averageMark) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", quest_id=" + quest_id +
                ", usr_nickname='" + usr_nickname + '\'' +
                ", answr_id=" + answr_id +
                '}';
    }
}
