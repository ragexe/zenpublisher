package by.training.entity;

import by.training.entity.common.EnumEntityStatus;

import java.sql.Timestamp;

public class Question {
    private int id;
    private int categr_id;
    private String title;
    private String body;
    private String usr_nickname;
    private Timestamp datetime_crtn;
    private Timestamp date_upd;
    private EnumEntityStatus status;
    private String categoryName;
    private float averageMark;

    public Question() {
    }

    public Question(int id, int categr_id, String title, String body, String usr_nickname, Timestamp datetime_crtn, Timestamp date_upd, EnumEntityStatus status, String categoryName, float averageMark) {
        this.id = id;
        this.categr_id = categr_id;
        this.title = title;
        this.body = body;
        this.usr_nickname = usr_nickname;
        this.datetime_crtn = datetime_crtn;
        this.date_upd = date_upd;
        this.status = status;
        this.categoryName = categoryName;
        this.averageMark = averageMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategr_id() {
        return categr_id;
    }

    public void setCategr_id(int categr_id) {
        this.categr_id = categr_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUsr_nickname() {
        return usr_nickname;
    }

    public void setUsr_nickname(String usr_nickname) {
        this.usr_nickname = usr_nickname;
    }

    public Timestamp getDatetime_crtn() {
        return datetime_crtn;
    }

    public void setDatetime_crtn(Timestamp datetime_crtn) {
        this.datetime_crtn = datetime_crtn;
    }

    public Timestamp getDate_upd() {
        return date_upd;
    }

    public void setDate_upd(Timestamp date_upd) {
        this.date_upd = date_upd;
    }

    public EnumEntityStatus getStatus() {
        return status;
    }

    public void setStatus(EnumEntityStatus status) {
        this.status = status;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public float getAverageMark() {
        return averageMark;
    }

    public void setAverageMark(float averageMark) {
        this.averageMark = averageMark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;

        Question question = (Question) o;

        if (id != question.id) return false;
        if (categr_id != question.categr_id) return false;
        if (Float.compare(question.averageMark, averageMark) != 0) return false;
        if (title != null ? !title.equals(question.title) : question.title != null) return false;
        if (body != null ? !body.equals(question.body) : question.body != null) return false;
        if (usr_nickname != null ? !usr_nickname.equals(question.usr_nickname) : question.usr_nickname != null)
            return false;
        if (datetime_crtn != null ? !datetime_crtn.equals(question.datetime_crtn) : question.datetime_crtn != null)
            return false;
        if (date_upd != null ? !date_upd.equals(question.date_upd) : question.date_upd != null) return false;
        if (status != question.status) return false;
        return categoryName != null ? categoryName.equals(question.categoryName) : question.categoryName == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + categr_id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (body != null ? body.hashCode() : 0);
        result = 31 * result + (usr_nickname != null ? usr_nickname.hashCode() : 0);
        result = 31 * result + (datetime_crtn != null ? datetime_crtn.hashCode() : 0);
        result = 31 * result + (date_upd != null ? date_upd.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (categoryName != null ? categoryName.hashCode() : 0);
        result = 31 * result + (averageMark != +0.0f ? Float.floatToIntBits(averageMark) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", categr_id=" + categr_id +
                ", usr_nickname='" + usr_nickname + '\'' +
                ", datetime_crtn=" + datetime_crtn +
                ", date_upd=" + date_upd +
                '}';
    }
}
