package by.training.entity;

import by.training.entity.common.EnumEntityStatus;

import java.sql.Date;
import java.sql.Timestamp;

public class User {
    private String nickname;
    private EnumUserRole role;
    private Date date_crtn;
    private Timestamp date_lactivity;
    private EnumEntityStatus status;
    private String password;
    private boolean isValid = false;
    private float averageFame;

    public enum EnumUserRole{
        ADMIN, COMMON
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public EnumUserRole getRole() {
        return role;
    }
    public void setRole(EnumUserRole role) {
        this.role = role;
    }

    public Date getDate_crtn() {
        return date_crtn;
    }
    public void setDate_crtn(Date date_crtn) {
        this.date_crtn = date_crtn;
    }

    public Timestamp getDate_lactivity() {
        return date_lactivity;
    }
    public void setDate_lactivity(Timestamp date_lactivity) {
        this.date_lactivity = date_lactivity;
    }

    public EnumEntityStatus getStatus() {
        return status;
    }
    public void setStatus(EnumEntityStatus status) {
        this.status = status;
    }

    public boolean isValid() {
        return isValid;
    }
    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public float getAverageFame() {
        return averageFame;
    }
    public void setAverageFame(float averageFame) {
        this.averageFame = averageFame;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (isValid != user.isValid) return false;
        if (nickname != null ? !nickname.equals(user.nickname) : user.nickname != null) return false;
        if (role != user.role) return false;
        if (date_crtn != null ? !date_crtn.equals(user.date_crtn) : user.date_crtn != null) return false;
        if (date_lactivity != null ? !date_lactivity.equals(user.date_lactivity) : user.date_lactivity != null)
            return false;
        if (status != user.status) return false;
        return password != null ? password.equals(user.password) : user.password == null;

    }

    @Override
    public int hashCode() {
        int result = nickname != null ? nickname.hashCode() : 0;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (date_crtn != null ? date_crtn.hashCode() : 0);
        result = 31 * result + (date_lactivity != null ? date_lactivity.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (isValid ? 1 : 0);
        result = 31 * result + (averageFame != +0.0f ? Float.floatToIntBits(averageFame) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "nickname='" + nickname + '\'' +
                ", role=" + role +
                ", isValid=" + isValid +
                ", status=" + status +
                '}';
    }
}
