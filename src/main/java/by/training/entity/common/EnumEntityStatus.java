package by.training.entity.common;

public enum EnumEntityStatus {
    DELETED(true),
    SAVED(false);

    boolean deleted;

    EnumEntityStatus(boolean status){
        this.deleted = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public static EnumEntityStatus getStatus(byte i){
        if (i == 0) return SAVED;
        return DELETED;
    }

}
