package by.training.dto;

import by.training.entity.Answer;

import java.util.ArrayList;
import java.util.List;

public class AnswerDTO extends Answer{

    private List<AnswerDTO> answerChildList = new ArrayList<>();

    public AnswerDTO() {
        this.setId(-1);
        this.setQuest_id(-2);
        this.setAnswr_id(-3);
    }

    public AnswerDTO(Answer answer) {
        this.setAnswr_id(answer.getAnswr_id());
        this.setBody(answer.getBody());
        this.setDatetime_upd(answer.getDatetime_upd());
        this.setDatetime_crtn(answer.getDatetime_crtn());
        this.setId(answer.getId());
        this.setStatus(answer.getStatus());
        this.setUsr_nickname(answer.getUsr_nickname());
        this.setQuest_id(answer.getQuest_id());
        this.setAverageMark(answer.getAverageMark());
    }

    public List<AnswerDTO> getAnswerChildList() {
        return answerChildList;
    }

    public void addChild(Answer answer){
        answerChildList.add(new AnswerDTO(answer));
    }

    @Override
    public String toString() {
        return "AnswerDTO{id:" + id + ", " +
                "answerChildList=" + answerChildList +
                "} ";
    }


}
