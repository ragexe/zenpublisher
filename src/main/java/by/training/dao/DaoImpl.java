package by.training.dao;

import by.training.entity.Answer;
import by.training.entity.Category;
import by.training.entity.Question;
import by.training.entity.User;
import by.training.entity.common.EnumEntityStatus;
import by.training.dao.exception.DaoException;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DaoImpl implements IDao {
    private DataSource dataSource = null;

    private int quantityOfRecords;

    private static final Logger logger = Logger.getLogger(DaoImpl.class);

    public DaoImpl(){
        try {
            InitialContext initContext = new InitialContext();
            dataSource = (DataSource) initContext.lookup("java:comp/env/jdbc/mysqlserver");
        } catch (NamingException e) {
            logger.fatal(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Categories methods.">
    @Override
    public List<Category> getAllCategories() throws DaoException {
        List<Category> categoryList = new ArrayList<>();
        //language=MySQL

        ResultSet resultSet;
        Statement statement;

        String query = "SELECT categr_id, categr_name, categr_del FROM category WHERE categr_del <> 1";

        try (Connection connection = dataSource.getConnection()){

            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                Category category = new Category();

                category.setId(resultSet.getInt("categr_id"));
                category.setName(resultSet.getString("categr_name"));
                category.setStatus(EnumEntityStatus.getStatus(resultSet.getByte("categr_del")));

                categoryList.add(category);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }

        return categoryList;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Users methods.">
    @Override
    public List<User> getAllUsers() throws DaoException {
        List<User> resultList = new ArrayList<>();
        String query = "SELECT usr_nickname, usr_role, usr_date_crtn, usr_date_lactivity, usr_del FROM user WHERE usr_del <> 1";
        ResultSet resultSet;
        Statement statement;

        try (Connection connection = dataSource.getConnection()){

            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                User user = new User();

                user.setNickname(resultSet.getString("usr_nickname"));
                user.setRole(User.EnumUserRole.valueOf(resultSet.getString("usr_role")));
                user.setDate_crtn(resultSet.getDate("usr_date_crtn"));
                user.setDate_lactivity(resultSet.getTimestamp("usr_date_lactivity"));
                user.setStatus(EnumEntityStatus.getStatus(resultSet.getByte("usr_del")));
                resultList.add(user);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
        return resultList;
    }

    @Override
    public boolean isUserExist(String name) throws DaoException {
        //language=MySQL
        String query = "SELECT * FROM user WHERE usr_nickname = '" + name + "' AND usr_del <> 1 ";
        ResultSet resultSet;
        Statement statement;

        try (Connection connection = dataSource.getConnection()) {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }

        return false;
    }

    @Override
    public float getUserFame(String nickname) throws DaoException {
        float result = 0;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try(Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("SELECT sum(mark)/sum(count) as average_mark FROM " +
                    "( " +
                    "SELECT sum(fq.fame_quest_mark) AS mark, count(fq.fame_quest_mark) as count " +
                    "FROM question q " +
                    "LEFT JOIN fame_quest fq ON q.quest_id = fq.fame_quest_quest_id " +
                    "WHERE quest_usr_nickname=? " +
                    "UNION " +
                    "( " +
                    "SELECT sum(fa.fame_answr_mark) AS mark, count(fa.fame_answr_mark) as count " +
                    "FROM answer a " +
                    "LEFT JOIN fame_answer fa ON a.answr_id = fa.fame_answr_answr_id " +
                    "where answr_usr_nickname=? " +
                    ") " +
                    ") x");
            preparedStatement.setString(1, nickname);
            preparedStatement.setString(2, nickname);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getFloat("average_mark");
            }

        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }

        return result;
    }

    @Override
    public User getUser(String name, String password) throws DaoException {
        User user = new User();

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()) {
            preparedStatement = connection.prepareStatement("SELECT usr_nickname, usr_role, usr_date_lactivity, usr_date_crtn, usr_del " +
                    "FROM user " +
                    "WHERE usr_nickname = ? AND usr_password = MD5(?) AND usr_del <> 1 ");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user.setValid(true);
                user.setStatus(EnumEntityStatus.getStatus(resultSet.getByte("usr_del")));
                user.setRole(User.EnumUserRole.valueOf(resultSet.getString("usr_role")));
                user.setDate_crtn(resultSet.getDate("usr_date_crtn"));
                user.setNickname(resultSet.getString("usr_nickname"));
                user.setDate_lactivity(resultSet.getTimestamp("usr_date_lactivity"));
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }

        return user;
    }

    @Override
    public void saveUser(User user) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try(Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("INSERT INTO user (`usr_nickname`, `usr_role`, `usr_date_crtn`, `usr_password`) VALUES (?, ?, ?, MD5(?))");
            preparedStatement.setString(1, user.getNickname());
            preparedStatement.setString(2, user.getRole().toString());
            preparedStatement.setDate(3, user.getDate_crtn());
            preparedStatement.setString(4, user.getPassword());

            result = preparedStatement.executeUpdate();
            if (result > 2){
                connection.rollback();
                throw new DaoException("logic error: updated more than one row");
            }

            connection.commit();
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Questions methods.">
    @Override
    public void updateQuestMark(String user, int questId, short mark, Timestamp timestamp) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try(Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("INSERT INTO fame_quest (`fame_quest_quest_id`," +
                    " `fame_quest_usr_nickname`, `fame_quest_mark`, `fame_quest_datetime`) " +
                    "VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `fame_quest_mark`=?, `fame_quest_datetime`=?");
            preparedStatement.setInt(1, questId);
            preparedStatement.setString(2, user);
            preparedStatement.setInt(3, mark);
            preparedStatement.setTimestamp(4, timestamp);
            preparedStatement.setInt(5, mark);
            preparedStatement.setTimestamp(6, timestamp);

            result = preparedStatement.executeUpdate();

            if (result > 2){
                connection.rollback();
                throw new DaoException("logic error: updated more than one row");
            }
            connection.commit();

        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
    }

    @Override
    public int saveQuest(Question question) throws DaoException {
        int result;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("INSERT INTO question (`quest_categr_id`, " +
                    "`quest_title`, `quest_body`, `quest_usr_nickname`, `quest_datetime_crtn`) " +
                    "VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, question.getCategr_id());
            preparedStatement.setString(2, question.getTitle());
            preparedStatement.setString(3, question.getBody());
            preparedStatement.setString(4, question.getUsr_nickname());
            preparedStatement.setTimestamp(5, question.getDatetime_crtn());

            result = preparedStatement.executeUpdate();
            if (result != 1){
                throw new DaoException("logic error");
            }

            resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
        return result;
    }

    @Override
    public void deleteQuestion(int questId) throws DaoException {

        PreparedStatement preparedStatement;

        int result;
        try(Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("UPDATE question SET `quest_del`=? WHERE `quest_id`=?");
            preparedStatement.setByte(1, (byte) 1);
            preparedStatement.setInt(2, questId);

            result = preparedStatement.executeUpdate();
            if (result > 1){
                connection.rollback();
                throw new DaoException("logic error: updated more than one row");
            }

            connection.commit();
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
    }

    @Override
    public List<Question> getAllQuestions(int offset, int numOfRecord) throws DaoException {
        List<Question> resultList = new ArrayList<>();

        ResultSet resultSet;
        Statement statement;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){

            statement = connection.createStatement();

            preparedStatement = connection.prepareStatement("SELECT SQL_CALC_FOUND_ROWS quest_id, quest_categr_id, quest_title, quest_body, " +
                    "quest_usr_nickname, quest_datetime_crtn, quest_date_upd, quest_del, c.categr_name, " +
                    "sum(fq.fame_quest_mark)/count(fq.fame_quest_mark) AS average_mark " +
                    "FROM question q JOIN category c ON quest_categr_id = categr_id " +
                    "LEFT JOIN fame_quest fq ON q.quest_id = fq.fame_quest_quest_id " +
                    "WHERE quest_del <> 1 " +
                    "GROUP BY quest_id " +
                    "ORDER BY quest_datetime_crtn DESC " +
                    "LIMIT ?, ?");
            preparedStatement.setInt(1, numOfRecord);
            preparedStatement.setInt(2, offset);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Question question = initQuestion(resultSet);
                resultList.add(question);
            }

            resultSet.close();
            resultSet = statement.executeQuery("SELECT FOUND_ROWS()");
            if(resultSet.next()) {
                this.quantityOfRecords = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }

        return resultList;
    }

    @Override
    public List<Question> getAllQuestionsByCategrId(int categrId, int offset, int numOfRecord) throws DaoException {
        List<Question> resultList = new ArrayList<>();

        ResultSet resultSet;
        Statement statement;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){

            preparedStatement = connection.prepareStatement("SELECT SQL_CALC_FOUND_ROWS quest_id, quest_categr_id, quest_title, quest_body, quest_usr_nickname, " +
                    "quest_datetime_crtn, quest_date_upd, quest_del, c.categr_name, " +
                    "sum(fq.fame_quest_mark)/count(fq.fame_quest_mark) AS average_mark " +
                    "FROM question q " +
                    "JOIN category c ON quest_categr_id = categr_id " +
                    "LEFT JOIN fame_quest fq ON q.quest_id = fq.fame_quest_quest_id " +
                    "WHERE quest_del <> 1 " + "AND " + " quest_categr_id = ? " +
                    "GROUP BY quest_id " +
                    "ORDER BY quest_datetime_crtn DESC " +
                    "LIMIT ?, ?");
            preparedStatement.setInt(1, categrId);
            preparedStatement.setInt(2, numOfRecord);
            preparedStatement.setInt(3, offset);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Question question = initQuestion(resultSet);
                resultList.add(question);
            }

            resultSet.close();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT FOUND_ROWS()");
            if(resultSet.next()) {
                this.quantityOfRecords = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }

        return resultList;
    }

    @Override
    public Question getQuestionById(int questionId) throws DaoException {
        Question question = null;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("SELECT quest_id, quest_categr_id, quest_title, quest_body, " +
                    "quest_usr_nickname, quest_datetime_crtn, quest_date_upd, quest_del, c.categr_name, " +
                    "sum(fq.fame_quest_mark)/count(fq.fame_quest_mark) AS average_mark " +
                    "FROM question q " +
                    "JOIN category c ON quest_categr_id = categr_id " +
                    "LEFT JOIN fame_quest fq ON q.quest_id = fq.fame_quest_quest_id " +
                    "WHERE quest_del <> 1 AND quest_id = ? " +
                    "GROUP BY quest_id");
            preparedStatement.setInt(1, questionId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                question = this.initQuestion(resultSet);
            }

            return question;
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }

    }

    @Override
    public int updateQuest(Question question) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("UPDATE question SET `quest_categr_id`=?, " +
                    "`quest_title`=?, `quest_body`=?, `quest_date_upd`=? " +
                    "WHERE `quest_id`=?");
            preparedStatement.setInt(1, question.getCategr_id());
            preparedStatement.setString(2, question.getTitle());
            preparedStatement.setString(3, question.getBody());
            preparedStatement.setTimestamp(4, question.getDate_upd());
            preparedStatement.setInt(5, question.getId());

            result = preparedStatement.executeUpdate();

            if (result != 1){
                throw new DaoException("logic error: updated " + result + " row(s)");
            }

        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }
        return result;
    }

    private Question initQuestion(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt("quest_id");
        int categr_id = resultSet.getInt("quest_categr_id");
        String title = resultSet.getString("quest_title");
        String body = resultSet.getString("quest_body");
        String usr_nickname = resultSet.getString("quest_usr_nickname");
        Timestamp datetime_crtn = resultSet.getTimestamp("quest_datetime_crtn", Calendar.getInstance());
        Timestamp date_upd = resultSet.getTimestamp("quest_date_upd");
        EnumEntityStatus status = EnumEntityStatus.getStatus(resultSet.getByte("quest_del"));
        String categr_name = resultSet.getString("categr_name");
        float average_mark = resultSet.getFloat("average_mark");

        return new Question(id, categr_id, title, body, usr_nickname, datetime_crtn, date_upd, status, categr_name, average_mark);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Answers methods.">
    @Override
    public void deleteAnswer(int answerId) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try(Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("UPDATE answer SET `answr_del`=? WHERE `answr_id`=?");
            preparedStatement.setByte(1, (byte) 1);
            preparedStatement.setInt(2, answerId);

            result = preparedStatement.executeUpdate();
            if (result > 1){
                connection.rollback();
                throw new DaoException("logic error: updated more than one row");
            }

            connection.commit();
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }

    }

    @Override
    public void updateAnswer(Answer answer) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("UPDATE answer " +
                    "SET `answr_body`=?, `answr_datetime_upd`=? WHERE `answr_id`=?");
            preparedStatement.setString(1, answer.getBody());
            preparedStatement.setTimestamp(2, answer.getDatetime_upd());
            preparedStatement.setInt(3, answer.getId());

            result = preparedStatement.executeUpdate();

            if (result != 1){
                throw new DaoException("logic error: updated " + result + " row(s)");
            }

        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }
    }

    @Override
    public List<Answer> getAnswersByQuestId(int questionId) throws DaoException {
        List<Answer> answers;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            answers = new ArrayList<>();
            preparedStatement = connection.prepareStatement("SELECT answr_id, answr_usr_nickname, answr_answr_id, " +
                    "answr_datetime_crtn, answr_body, answr_datetime_upd, answr_del, answr_quest_id," +
                    "sum(fa.fame_answr_mark)/count(fa.fame_answr_mark) AS average_mark " +
                    "FROM answer a " +
                    "LEFT JOIN fame_answer fa ON a.answr_id = fa.fame_answr_answr_id " +
                    "WHERE answr_del <> 1 AND answr_quest_id = ? " +
                    "GROUP BY answr_id");
            preparedStatement.setInt(1, questionId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Answer answer = initAnswer(resultSet);

                answers.add(answer);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }
        return answers;
    }

    @Override
    public Answer getAnswerById(int questionId) throws DaoException {
        Answer answer = null;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("SELECT answr_id, answr_usr_nickname, answr_answr_id, " +
                    "answr_datetime_crtn, answr_body, answr_datetime_upd, answr_del, answr_quest_id, " +
                    "sum(fa.fame_answr_mark)/count(fa.fame_answr_mark) AS average_mark " +
                    "FROM answer a " +
                    "LEFT JOIN fame_answer fa ON a.answr_id = fa.fame_answr_answr_id " +
                    "WHERE a.answr_del <> 1 AND a.answr_id = ? " +
                    "GROUP BY answr_id");
            preparedStatement.setInt(1, questionId);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                answer = initAnswer(resultSet);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException();
        }
        return answer;
    }

    @Override
    public int saveAnswer(Answer answer) throws DaoException {
        int result;

        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try (Connection connection = dataSource.getConnection()){
            preparedStatement = connection.prepareStatement("INSERT INTO answer (`answr_usr_nickname`, " +
                    "`answr_answr_id`, `answr_quest_id`, `answr_datetime_crtn`, `answr_body`) " +
                    "VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, answer.getUsr_nickname());
            preparedStatement.setInt(2, answer.getAnswr_id());
            preparedStatement.setInt(3, answer.getQuest_id());
            preparedStatement.setTimestamp(4, answer.getDatetime_crtn());
            preparedStatement.setString(5, answer.getBody());

            result = preparedStatement.executeUpdate();
            if (result != 1){
                throw new DaoException("logic error: updated " + result + " row(s)");
            }

            resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
        return result;
    }

    @Override
    public void updateAnswerMark(String user, int answerId, short mark, Timestamp timestamp) throws DaoException {
        int result;

        PreparedStatement preparedStatement;

        try(Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement("INSERT INTO fame_answer (`fame_answr_answr_id`, " +
                    "`fame_answr_usr_nickname`, `fame_answr_mark`, `fame_answr_datetime`) " +
                    "VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `fame_answr_mark`=?, `fame_answr_datetime`=?;");
            preparedStatement.setInt(1, answerId);
            preparedStatement.setString(2, user);
            preparedStatement.setInt(3, mark);
            preparedStatement.setTimestamp(4, timestamp);
            preparedStatement.setInt(5, mark);
            preparedStatement.setTimestamp(6, timestamp);

            result = preparedStatement.executeUpdate();
            if (result > 2){
                connection.rollback();
                throw new DaoException("logic error: updated more than one row");
            }
            connection.commit();

        } catch (SQLException e) {
            logger.error(e);
            throw new DaoException(e);
        }
    }

    private Answer initAnswer(ResultSet resultSet) throws SQLException {

        int id = resultSet.getInt("answr_id");
        String usr_nickname = resultSet.getString("answr_usr_nickname");
        int answr_id = resultSet.getInt("answr_answr_id");
        int quest_id = resultSet.getInt("answr_quest_id");
        Timestamp datetime_crtn = resultSet.getTimestamp("answr_datetime_crtn");
        String body = resultSet.getString("answr_body");
        Timestamp datetime_upd = resultSet.getTimestamp("answr_datetime_upd");
        EnumEntityStatus status = EnumEntityStatus.getStatus(resultSet.getByte("answr_del"));
        float averageMark = resultSet.getFloat("average_mark");

        return new Answer(id, usr_nickname, answr_id, quest_id, datetime_crtn, body, datetime_upd, status, averageMark);
    }
    // </editor-fold>

    public int getQuantityOfRecords() {
        return quantityOfRecords;
    }
}
