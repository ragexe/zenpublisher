package by.training.dao;

import by.training.entity.Answer;
import by.training.entity.Category;
import by.training.entity.Question;
import by.training.entity.User;
import by.training.dao.exception.DaoException;

import java.sql.Timestamp;
import java.util.List;

public interface IDao {
    List<Question> getAllQuestions(int offset, int numOfRecord) throws DaoException;
    List<Question> getAllQuestionsByCategrId(int categrId, int offset, int numOfRecord) throws DaoException;
    Question getQuestionById(int questionId) throws DaoException;
    List<Answer> getAnswersByQuestId(int questionId) throws DaoException;
    Answer getAnswerById(int answerId) throws DaoException;
    List<Category> getAllCategories() throws DaoException;
    List<User> getAllUsers() throws DaoException;
    User getUser(String name, String password) throws DaoException;
    boolean isUserExist(String name) throws DaoException;
    float getUserFame(String nickname) throws DaoException;
    int saveQuest(Question question) throws DaoException;
    int updateQuest(Question question) throws DaoException;
    int saveAnswer(Answer answer) throws DaoException;
    void updateQuestMark(String user, int questId, short mark, Timestamp timestamp) throws DaoException;
    void updateAnswerMark(String user, int questId, short mark, Timestamp timestamp) throws DaoException;
    void saveUser(User user) throws DaoException;
    void deleteAnswer(int answerId) throws DaoException;
    void deleteQuestion(int questId) throws DaoException;
    void updateAnswer(Answer answer) throws DaoException;

    int getQuantityOfRecords();

//    boolean isCategoryExist(int categr_id) throws DaoException;
}
