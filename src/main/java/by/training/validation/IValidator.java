package by.training.validation;

import by.training.entity.Answer;
import by.training.entity.Question;
import by.training.validation.exception.ValidatorBundledMessagesException;
import by.training.service.IService;
import by.training.validation.exception.ValidatorException;

import java.util.List;

public interface IValidator {
    String validateLanguage(String language);

    boolean isValidNewUserData(IService service, String nickname, String password, String passwordR) throws ValidatorBundledMessagesException;

    boolean isValidQuestionToSave(IService service, Question question) throws ValidatorException;

    boolean isValidEditedAnswerToUpdate(IService service, Answer answer) throws ValidatorException;

    boolean isValidAnswerToSave(IService service, Answer answer) throws ValidatorException;

    boolean isValidAnswerMark(IService service, int answerId, short mark) throws ValidatorException;

    boolean isValidQuestionMark(IService service, int questId, short mark) throws ValidatorException;

    List<Answer> validateListOfAnswers(List<Answer> answers);
}
