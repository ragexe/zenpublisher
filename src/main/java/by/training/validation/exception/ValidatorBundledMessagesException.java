package by.training.validation.exception;

import by.training.validation.ValidatorImpl;

import java.util.ResourceBundle;

public class ValidatorBundledMessagesException extends Exception {
    private static final String NAME_BUNDLE_FILE = "text";

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(NAME_BUNDLE_FILE);
    private ValidatorImpl.KeysEnum keyOfMessage;

    public ValidatorBundledMessagesException(ValidatorImpl.KeysEnum keysEnum) {
        super(RESOURCE_BUNDLE.getString(keysEnum.getKey()));
        this.keyOfMessage = keysEnum;
    }

    public String getKeyOfMessage() {
        return keyOfMessage.getKey();
    }

    @Override
    public String getLocalizedMessage(){
        return RESOURCE_BUNDLE.getString(keyOfMessage.getKey());
    }
}
