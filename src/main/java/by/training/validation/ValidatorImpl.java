package by.training.validation;

import by.training.entity.Answer;
import by.training.entity.Question;
import by.training.service.IService;
import by.training.service.exception.ServiceException;
import by.training.validation.exception.ValidatorBundledMessagesException;
import by.training.validation.exception.ValidatorException;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorImpl implements IValidator {
    private static final Logger logger = Logger.getLogger(ValidatorImpl.class);

    private static final int BODY_MAX_LENGTH = 255;
    private static final int TITLE_MAX_LENGTH = 63;

    public enum KeysEnum {
        NICKPASS_INV("validator.signup.nickpass_inv"),
        NICK_INVTAKEN("validator.signup.nick_invtaken"),
        NICK_INV("validator.signup.nick_inv"),
        PASS_MATCH("validator.signup.pass_match"),
        PASS_INV("validator.signup.pass_inv");

        private String key;

        KeysEnum(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    @Override
    public boolean isValidNewUserData(IService service, String nickname, String password, String passwordR) throws ValidatorBundledMessagesException {
        Pattern pattern = Pattern.compile("[_A-Za-z0-9-]{2,15}");

        if (password == null || password.isEmpty() || nickname == null || nickname.isEmpty()) {
            throw new ValidatorBundledMessagesException(KeysEnum.NICKPASS_INV);
        }

        try {
            if (service.isUserExist(nickname)) {
                throw new ValidatorBundledMessagesException(KeysEnum.NICK_INVTAKEN);
            }
        } catch (ServiceException e) {
            logger.error(e);
            throw new ValidatorBundledMessagesException(KeysEnum.NICK_INV);
        }

        if (passwordR == null || !password.equals(passwordR)) {
            throw new ValidatorBundledMessagesException(KeysEnum.PASS_MATCH);
        }


        Matcher matcherNickname = pattern.matcher(nickname);
        if (!matcherNickname.matches()) {
            throw new ValidatorBundledMessagesException(KeysEnum.NICK_INV);
        }

        Matcher matcherPassword = pattern.matcher(password);
        if (!matcherPassword.matches()) {
            throw new ValidatorBundledMessagesException(KeysEnum.PASS_INV);
        }

        return true;
    }

    @Override
    public String validateLanguage(String language) {
        language = language.toLowerCase();
        switch (language) {
            case "ru": {
                break;
            }
            case "en": {
                break;
            }
            case "be": {
                break;
            }
            default: {
                logger.info("Get wrong language from client - \"" + language + "\". Setting \"ru\"...");
                return "ru";
            }
        }
        return language;
    }

    @Override
    public boolean isValidQuestionToSave(IService service, Question question) throws ValidatorException {
        try {
            if (!service.isCategoryExist(question.getCategr_id())) {
                throw new ValidatorException("Bad category id");
            }

            if (question.getTitle().isEmpty() || question.getTitle().length() > TITLE_MAX_LENGTH) {
                throw new ValidatorException("Bad title of question");
            }

            if (question.getBody().isEmpty() || question.getBody().length() > BODY_MAX_LENGTH) {
                throw new ValidatorException("Bad body of question");
            }
        } catch (ServiceException e) {
            logger.error(e);
            throw new ValidatorException(e);
        }

        return true;
    }

    @Override
    public boolean isValidEditedAnswerToUpdate(IService service, Answer answer) throws ValidatorException {
        if (answer.getBody().isEmpty() || answer.getBody().length() > BODY_MAX_LENGTH) {
            throw new ValidatorException("Bad body of answer");
        }
        return true;
    }

    @Override
    public boolean isValidAnswerToSave(IService service, Answer answer) throws ValidatorException {
        try {
            if (service.getQuestionById(answer.getQuest_id()) == null) {
                throw new ValidatorException("Bad id of question");
            }

            if (answer.getBody().length() > BODY_MAX_LENGTH) {
                throw new ValidatorException("Bad body of answer");
            }

            int idOfParentAnswer = answer.getAnswr_id();

            if (idOfParentAnswer == -1) {
                return true;
            } else {
                for (Answer answer1 : service.getAnswersByQuestId(answer.getQuest_id())) {
                    if (idOfParentAnswer == answer1.getId()) {
                        return true;
                    }
                }
            }
            throw new ValidatorException("Bad parent-answer id");
        } catch (ServiceException e) {
            logger.error(e);
            throw new ValidatorException(e);
        }
    }

    @Override
    public boolean isValidAnswerMark(IService service, int answerId, short mark) throws ValidatorException {
        try {
            if (service.getAnswerById(answerId) == null) {
                throw new ValidatorException("Bad answer id for estimating the answer");
            }

            if (mark > 10 || mark < 1) {
                throw new ValidatorException("Bad mark value for estimating answer");
            }
        } catch (ServiceException e) {
            logger.error(e);
            throw new ValidatorException(e);
        }
        return true;
    }

    @Override
    public boolean isValidQuestionMark(IService service, int questId, short mark) throws ValidatorException {
        try {
            if (service.getQuestionById(questId) == null) {
                throw new ValidatorException("Bad question id for estimating the question");
            }

            if (mark > 10 || mark < 1) {
                throw new ValidatorException("Bad mark value for estimating question");
            }
        } catch (ServiceException e) {
            logger.error(e);
            throw new ValidatorException(e);
        }
        return true;
    }

    @Override
    public List<Answer> validateListOfAnswers(List<Answer> answers) {
        boolean isValid = false;

        Iterator<Answer> iterator;
        Iterator<Answer> innerIterator;

        while (!answers.isEmpty() && !isValid) {
            iterator = answers.iterator();

            outerLoop:
            while (iterator.hasNext()) {
                Answer firstAnswer = iterator.next();

                isValid = false;

                innerIterator = answers.iterator();
                while (innerIterator.hasNext()) {
                    Answer secondAnswer = innerIterator.next();

                    if (firstAnswer.getAnswr_id() == -1 || firstAnswer.getAnswr_id() == secondAnswer.getId()) {
                        isValid = true;
                        continue outerLoop;
                    }
                }

                logger.warn(firstAnswer + " - is invalid parent id. Should be deleted!");
                iterator.remove();
            }
        }
        return answers;
    }
}



