package by.training.service.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PathHandler {
    private static final String NAME_OF_PROPERTY_FILE = "config.properties";
    private static String pathToRes;
    private static final Logger logger = Logger.getLogger(PathHandler.class);

    public enum PathEnum {
        MAIN            ("path.page.main"),
        VIEWQ           ("path.page.viewQuestion"),
        QUEST_FORM      ("path.page.questform"),
        EDIT_QUEST_FORM ("path.page.edit_questform"),
        EDIT_ANSWER_FORM("path.page.edit_asnwerform"),
        SIGNUP_FORM     ("path.page.signupform"),
        ERROR_PAGE      ("path.page.error");

        private String key;

        PathEnum(String key) {
            this.key = key;
        }
        public String getKey() {
            return key;
        }
    }


    public static void setPathToRes(String pathToRes) {
        PathHandler.pathToRes = pathToRes;
    }

    public static String getProperty(String propertyName) {
        FileInputStream fileInputStream;
        Properties property = new Properties();
        String page = null;

        try {
            String path = pathToRes.concat(File.separator).concat(NAME_OF_PROPERTY_FILE);
            fileInputStream = new FileInputStream(path);
            property.load(fileInputStream);

            page = property.getProperty(propertyName);
        } catch (IOException e) {
            logger.fatal("Error: file *.property is not found! " + pathToRes.concat(File.separator).concat(NAME_OF_PROPERTY_FILE));
        }
        return page;
    }

    public static String getProperty(PathEnum pathEnum){
        return getProperty(pathEnum.getKey());
    }
}
