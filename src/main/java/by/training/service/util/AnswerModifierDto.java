package by.training.service.util;

import by.training.dto.AnswerDTO;
import by.training.entity.Answer;
import by.training.service.exception.ServiceException;
import by.training.validation.IValidator;
import by.training.validation.ValidatorImpl;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.List;

public class AnswerModifierDto {
    private static final Logger logger = Logger.getLogger(AnswerModifierDto.class);

    public static List<AnswerDTO> modify(List<Answer> answers) {
        AnswerDTO resultAnswerDTO = new AnswerDTO();

        IValidator validator = new ValidatorImpl();

        answers = validator.validateListOfAnswers(answers);

        Iterator<Answer> iterator;

        while (!answers.isEmpty()) {
            iterator = answers.iterator();

            while (iterator.hasNext()) {
                Answer answer = iterator.next();

                boolean success;
                try {
                    success = addChildByParentId(resultAnswerDTO, answer);
                } catch (ServiceException e) {
                    success = false;
                }
                if (success) {
                    iterator.remove();
                }

            }
        }

        return resultAnswerDTO.getAnswerChildList();
    }

    private static boolean addChildByParentId(AnswerDTO answerDTO, Answer answer) throws ServiceException {
        if (answerDTO.getId() == answer.getAnswr_id()) {
            answerDTO.addChild(answer);
            return true;
        } else {
            List<AnswerDTO> answerDTOChildList = answerDTO.getAnswerChildList();
            for (AnswerDTO childAnswerDTO : answerDTOChildList) {
                try {
                    return addChildByParentId(childAnswerDTO, answer);
                } catch (ServiceException e) {
                    //do nothing and skip
                }
            }
            throw new ServiceException();
        }
    }
}
