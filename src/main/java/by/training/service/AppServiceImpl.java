package by.training.service;

import by.training.dao.DaoImpl;
import by.training.dao.IDao;
import by.training.dto.AnswerDTO;
import by.training.entity.Answer;
import by.training.entity.Category;
import by.training.entity.Question;
import by.training.entity.User;
import by.training.dao.exception.DaoException;
import by.training.service.exception.ServiceException;
import by.training.service.util.AnswerModifierDto;
import by.training.validation.IValidator;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.List;

public class AppServiceImpl implements IService {
    private static final Logger logger = Logger.getLogger(AppServiceImpl.class);
    private IDao dao = new DaoImpl();
    private IValidator validator;

    @Override
    public List<Question> getAllQuestions(int offset, int numOfRecord) throws ServiceException {
        try {
            return dao.getAllQuestions(offset, numOfRecord);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Question> getAllQuestionsByCategrId(int categrId, int offset, int numOfRecord) throws ServiceException {
        try {
            return dao.getAllQuestionsByCategrId(categrId, offset, numOfRecord);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Question getQuestionById(int questionId) throws ServiceException {
        try {
            return dao.getQuestionById(questionId);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<AnswerDTO> getAnswersByQuestId(int questionId) throws ServiceException {
        try {
            return AnswerModifierDto.modify(dao.getAnswersByQuestId(questionId));
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Answer getAnswerById(int id) throws ServiceException {
        try {
            return dao.getAnswerById(id);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateAnswer(Answer answer) throws ServiceException{
        try {
            dao.updateAnswer(answer);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<User> getAllUsers() throws ServiceException {
        try {
            return dao.getAllUsers();
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException();
        }
    }


    @Override
    public List<Category> getAllCategories() throws ServiceException {
        try {
            return dao.getAllCategories();
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean isUserExist(String name) throws ServiceException {
        try {
            return dao.isUserExist(name);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User getUser(String name, String password) throws ServiceException {
        try {
            return dao.getUser(name, password);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int saveQuestion(Question question) throws ServiceException {
        try {
            return dao.saveQuest(question);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);

        }
    }

    @Override
    public int updateQuestion(Question question) throws ServiceException {
        try {
            return dao.updateQuest(question);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int saveAnswer(Answer answer) throws ServiceException {
        try {
            return dao.saveAnswer(answer);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateQuestMark(String user, int questId, short mark, Timestamp timestamp) throws ServiceException {
        try {
            dao.updateQuestMark(user, questId, mark, timestamp);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateAnswerMark(String user, int answerId, short mark, Timestamp timestamp) throws ServiceException {
        try {
            dao.updateAnswerMark(user, answerId, mark, timestamp);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void saveUser(User user) throws ServiceException {
        try {
            dao.saveUser(user);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public float getUserFame(String nickname) throws ServiceException {
        try {
            return dao.getUserFame(nickname);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAnswer(int answerId) throws ServiceException {
        try {
            dao.deleteAnswer(answerId);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteQuestion(int questId) throws ServiceException {
        try {
            dao.deleteQuestion(questId);
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int getQuantityOfRecords() {
        return dao.getQuantityOfRecords();
    }

    @Override
    public boolean isCategoryExist(int categr_id) throws ServiceException {
        boolean result = false;
        try {
            for (Category category : dao.getAllCategories()) {
                if (category.getId() == categr_id){
                    result = true;
                }
            }
        } catch (DaoException e) {
            logger.error(e);
            throw new ServiceException();
        }
        return result;
    }
}
