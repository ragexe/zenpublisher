package by.training.service.exception;

public class ModifyDtoException extends Throwable {
    public ModifyDtoException() {
    }

    public ModifyDtoException(String message) {
        super(message);
    }

    public ModifyDtoException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModifyDtoException(Throwable cause) {
        super(cause);
    }

    public ModifyDtoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
