package by.training.service;

import by.training.dto.AnswerDTO;
import by.training.entity.Answer;
import by.training.entity.Category;
import by.training.entity.Question;
import by.training.entity.User;
import by.training.service.exception.ServiceException;

import java.sql.Timestamp;
import java.util.List;

public interface IService {
    List<Question> getAllQuestions(int offset, int numOfRecord) throws ServiceException;
    List<Question> getAllQuestionsByCategrId(int categrId, int offset, int numOfRecord) throws ServiceException;
    Question getQuestionById(int questionId) throws ServiceException;
    int saveQuestion(Question question) throws ServiceException;
    void deleteQuestion(int questId) throws ServiceException;
    int updateQuestion(Question question) throws ServiceException;
    void updateQuestMark(String user, int questId, short mark, Timestamp timestamp) throws ServiceException;

    List<User> getAllUsers() throws ServiceException;
    User getUser(String name, String password) throws ServiceException;
    boolean isUserExist(String name) throws ServiceException;
    void saveUser(User user) throws ServiceException;
    float getUserFame(String nickname) throws ServiceException;

    List<Category> getAllCategories() throws ServiceException;

    List<AnswerDTO> getAnswersByQuestId(int questionId) throws ServiceException;
    int saveAnswer(Answer answer) throws ServiceException;
    Answer getAnswerById(int id) throws ServiceException;
    void updateAnswer(Answer answer) throws ServiceException;
    void deleteAnswer(int answerId) throws ServiceException;
    void updateAnswerMark(String user, int answerId, short mark, Timestamp timestamp) throws ServiceException;

    int getQuantityOfRecords();

    boolean isCategoryExist(int categr_id) throws ServiceException;
}
