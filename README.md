zenPublisher Web Application

[Пример работы](http://ragexe.mycloud.by/zenPublisher/main)

1.  log4j.properties - path to *.log file;

2.  ////----- Java Authentication and Authorization Service
    //<CATALINA_HOME>/conf/jaas.config
    //--------- jaas.config
    ZenPublisherLogin {
    	by.training.security.LoginModuleImpl REQUIRED debug=true;
    };
    
3.  // <CATALINA_HOME>/bin/catalina.bat
    // after ":okHome" ~127 line
    set JAVA_OPTS=%JAVA_OPTS% -Djava.security.auth.login.config=%CATALINA_HOME%/conf/jaas.config
    set "EXECUTABLE=%CATALINA_HOME%\bin\catalina.bat"

4.  MySQL database dump
5.  META-INF/context.xml configurate username, password, name of database